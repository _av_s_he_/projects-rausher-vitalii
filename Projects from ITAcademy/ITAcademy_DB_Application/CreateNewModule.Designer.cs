﻿namespace ITAcademy_DB_Application
{
    partial class CreateNewModule
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnMainMenu = new System.Windows.Forms.Button();
            this.cmbTreningModule = new System.Windows.Forms.ComboBox();
            this.dgvTreningModule = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.txtNewModuleName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtEndDate = new System.Windows.Forms.TextBox();
            this.btnCreateNewModule = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.txtNewTestName = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.cmbModuleRewrite = new System.Windows.Forms.ComboBox();
            this.txtModuleNameRewrite = new System.Windows.Forms.TextBox();
            this.txtStartModuleRewrite = new System.Windows.Forms.TextBox();
            this.txtEndModuleRewrite = new System.Windows.Forms.TextBox();
            this.cmbTreningIDModuleRewrite = new System.Windows.Forms.ComboBox();
            this.btnRewrite = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTreningModule)).BeginInit();
            this.SuspendLayout();
            // 
            // btnMainMenu
            // 
            this.btnMainMenu.BackColor = System.Drawing.SystemColors.Desktop;
            this.btnMainMenu.ForeColor = System.Drawing.Color.Lime;
            this.btnMainMenu.Location = new System.Drawing.Point(13, 13);
            this.btnMainMenu.Name = "btnMainMenu";
            this.btnMainMenu.Size = new System.Drawing.Size(134, 62);
            this.btnMainMenu.TabIndex = 0;
            this.btnMainMenu.Text = "<-- Назад в головне меню";
            this.btnMainMenu.UseVisualStyleBackColor = false;
            this.btnMainMenu.Click += new System.EventHandler(this.btnMainMenu_Click);
            // 
            // cmbTreningModule
            // 
            this.cmbTreningModule.BackColor = System.Drawing.SystemColors.Desktop;
            this.cmbTreningModule.ForeColor = System.Drawing.Color.Lime;
            this.cmbTreningModule.FormattingEnabled = true;
            this.cmbTreningModule.Location = new System.Drawing.Point(25, 137);
            this.cmbTreningModule.Name = "cmbTreningModule";
            this.cmbTreningModule.Size = new System.Drawing.Size(472, 23);
            this.cmbTreningModule.TabIndex = 1;
            this.cmbTreningModule.Text = "Виберіть тренінг для додавання нового модулю";
            this.cmbTreningModule.SelectedIndexChanged += new System.EventHandler(this.cmbTreningModule_SelectedIndexChanged);
            // 
            // dgvTreningModule
            // 
            this.dgvTreningModule.BackgroundColor = System.Drawing.SystemColors.Desktop;
            this.dgvTreningModule.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTreningModule.GridColor = System.Drawing.Color.Lime;
            this.dgvTreningModule.Location = new System.Drawing.Point(25, 166);
            this.dgvTreningModule.Name = "dgvTreningModule";
            this.dgvTreningModule.Size = new System.Drawing.Size(666, 488);
            this.dgvTreningModule.TabIndex = 2;
            this.dgvTreningModule.Text = "dataGridView1";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Lime;
            this.label1.Location = new System.Drawing.Point(717, 137);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(156, 15);
            this.label1.TabIndex = 3;
            this.label1.Text = "Введіть ім\'я нового модуля";
            // 
            // txtNewModuleName
            // 
            this.txtNewModuleName.BackColor = System.Drawing.SystemColors.Desktop;
            this.txtNewModuleName.ForeColor = System.Drawing.Color.Lime;
            this.txtNewModuleName.Location = new System.Drawing.Point(717, 156);
            this.txtNewModuleName.Name = "txtNewModuleName";
            this.txtNewModuleName.Size = new System.Drawing.Size(372, 23);
            this.txtNewModuleName.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Lime;
            this.label2.Location = new System.Drawing.Point(717, 186);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(224, 15);
            this.label2.TabIndex = 5;
            this.label2.Text = "Введіть дату закінчення даного модулю";
            // 
            // txtEndDate
            // 
            this.txtEndDate.BackColor = System.Drawing.SystemColors.Desktop;
            this.txtEndDate.ForeColor = System.Drawing.Color.Lime;
            this.txtEndDate.Location = new System.Drawing.Point(717, 205);
            this.txtEndDate.Name = "txtEndDate";
            this.txtEndDate.Size = new System.Drawing.Size(224, 23);
            this.txtEndDate.TabIndex = 6;
            // 
            // btnCreateNewModule
            // 
            this.btnCreateNewModule.BackColor = System.Drawing.SystemColors.Desktop;
            this.btnCreateNewModule.ForeColor = System.Drawing.Color.Lime;
            this.btnCreateNewModule.Location = new System.Drawing.Point(947, 186);
            this.btnCreateNewModule.Name = "btnCreateNewModule";
            this.btnCreateNewModule.Size = new System.Drawing.Size(142, 91);
            this.btnCreateNewModule.TabIndex = 7;
            this.btnCreateNewModule.Text = "Створити новий модуль";
            this.btnCreateNewModule.UseVisualStyleBackColor = false;
            this.btnCreateNewModule.Click += new System.EventHandler(this.btnCreateNewModule_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.Lime;
            this.label3.Location = new System.Drawing.Point(717, 235);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(212, 15);
            this.label3.TabIndex = 8;
            this.label3.Text = "Введіть ім\'я тесту для даного модулю";
            // 
            // txtNewTestName
            // 
            this.txtNewTestName.BackColor = System.Drawing.SystemColors.Desktop;
            this.txtNewTestName.ForeColor = System.Drawing.Color.Lime;
            this.txtNewTestName.Location = new System.Drawing.Point(717, 254);
            this.txtNewTestName.Name = "txtNewTestName";
            this.txtNewTestName.Size = new System.Drawing.Size(224, 23);
            this.txtNewTestName.TabIndex = 9;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.Lime;
            this.label4.Location = new System.Drawing.Point(717, 470);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(312, 15);
            this.label4.TabIndex = 8;
            this.label4.Text = "Виберіть тренінг до якого буде належати даний модуль";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.Lime;
            this.label5.Location = new System.Drawing.Point(717, 563);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(198, 15);
            this.label5.TabIndex = 5;
            this.label5.Text = "Відредагуйте дату початку модулю";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.Color.Lime;
            this.label6.Location = new System.Drawing.Point(717, 514);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(145, 15);
            this.label6.TabIndex = 3;
            this.label6.Text = "Відредагуйте ім\'я модуля";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.Color.Lime;
            this.label7.Location = new System.Drawing.Point(717, 612);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(214, 15);
            this.label7.TabIndex = 5;
            this.label7.Text = "Відредагуйте дату закінчення модулю";
            // 
            // cmbModuleRewrite
            // 
            this.cmbModuleRewrite.BackColor = System.Drawing.SystemColors.Desktop;
            this.cmbModuleRewrite.ForeColor = System.Drawing.Color.Lime;
            this.cmbModuleRewrite.FormattingEnabled = true;
            this.cmbModuleRewrite.Location = new System.Drawing.Point(717, 422);
            this.cmbModuleRewrite.Name = "cmbModuleRewrite";
            this.cmbModuleRewrite.Size = new System.Drawing.Size(372, 23);
            this.cmbModuleRewrite.TabIndex = 10;
            this.cmbModuleRewrite.Text = "Виберіть модуль для редагування";
            this.cmbModuleRewrite.SelectedIndexChanged += new System.EventHandler(this.cmbModuleRewrite_SelectedIndexChanged);
            // 
            // txtModuleNameRewrite
            // 
            this.txtModuleNameRewrite.BackColor = System.Drawing.SystemColors.Desktop;
            this.txtModuleNameRewrite.ForeColor = System.Drawing.Color.Lime;
            this.txtModuleNameRewrite.Location = new System.Drawing.Point(717, 533);
            this.txtModuleNameRewrite.Name = "txtModuleNameRewrite";
            this.txtModuleNameRewrite.Size = new System.Drawing.Size(372, 23);
            this.txtModuleNameRewrite.TabIndex = 11;
            // 
            // txtStartModuleRewrite
            // 
            this.txtStartModuleRewrite.BackColor = System.Drawing.SystemColors.Desktop;
            this.txtStartModuleRewrite.ForeColor = System.Drawing.Color.Lime;
            this.txtStartModuleRewrite.Location = new System.Drawing.Point(717, 582);
            this.txtStartModuleRewrite.Name = "txtStartModuleRewrite";
            this.txtStartModuleRewrite.Size = new System.Drawing.Size(224, 23);
            this.txtStartModuleRewrite.TabIndex = 12;
            // 
            // txtEndModuleRewrite
            // 
            this.txtEndModuleRewrite.BackColor = System.Drawing.SystemColors.Desktop;
            this.txtEndModuleRewrite.ForeColor = System.Drawing.Color.Lime;
            this.txtEndModuleRewrite.Location = new System.Drawing.Point(717, 631);
            this.txtEndModuleRewrite.Name = "txtEndModuleRewrite";
            this.txtEndModuleRewrite.Size = new System.Drawing.Size(224, 23);
            this.txtEndModuleRewrite.TabIndex = 13;
            // 
            // cmbTreningIDModuleRewrite
            // 
            this.cmbTreningIDModuleRewrite.BackColor = System.Drawing.SystemColors.Desktop;
            this.cmbTreningIDModuleRewrite.ForeColor = System.Drawing.Color.Lime;
            this.cmbTreningIDModuleRewrite.FormattingEnabled = true;
            this.cmbTreningIDModuleRewrite.Location = new System.Drawing.Point(717, 488);
            this.cmbTreningIDModuleRewrite.Name = "cmbTreningIDModuleRewrite";
            this.cmbTreningIDModuleRewrite.Size = new System.Drawing.Size(372, 23);
            this.cmbTreningIDModuleRewrite.TabIndex = 14;
            // 
            // btnRewrite
            // 
            this.btnRewrite.BackColor = System.Drawing.SystemColors.Desktop;
            this.btnRewrite.ForeColor = System.Drawing.Color.Lime;
            this.btnRewrite.Location = new System.Drawing.Point(947, 563);
            this.btnRewrite.Name = "btnRewrite";
            this.btnRewrite.Size = new System.Drawing.Size(142, 91);
            this.btnRewrite.TabIndex = 15;
            this.btnRewrite.Text = "Відредагувати модуль";
            this.btnRewrite.UseVisualStyleBackColor = false;
            this.btnRewrite.Click += new System.EventHandler(this.btnRewrite_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label8.ForeColor = System.Drawing.Color.Lime;
            this.label8.Location = new System.Drawing.Point(194, 13);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(735, 21);
            this.label8.TabIndex = 16;
            this.label8.Text = "Для створення модулю потрібно вибрати треніг на якому буде записуватись даний мод" +
    "уль";
            this.label8.Click += new System.EventHandler(this.label8_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label9.ForeColor = System.Drawing.Color.Lime;
            this.label9.Location = new System.Drawing.Point(194, 44);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(1053, 21);
            this.label9.TabIndex = 17;
            this.label9.Text = "Для редагування потрібно вибрати тренінг на якому даний тест знаходиться, після ч" +
    "ого обрати його для подальшого редагування";
            // 
            // CreateNewModule
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Desktop;
            this.ClientSize = new System.Drawing.Size(1290, 710);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.btnRewrite);
            this.Controls.Add(this.cmbTreningIDModuleRewrite);
            this.Controls.Add(this.txtEndModuleRewrite);
            this.Controls.Add(this.txtStartModuleRewrite);
            this.Controls.Add(this.txtModuleNameRewrite);
            this.Controls.Add(this.cmbModuleRewrite);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtNewTestName);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnCreateNewModule);
            this.Controls.Add(this.txtEndDate);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtNewModuleName);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dgvTreningModule);
            this.Controls.Add(this.cmbTreningModule);
            this.Controls.Add(this.btnMainMenu);
            this.Name = "CreateNewModule";
            this.Text = "CreateNewModule";
            this.Load += new System.EventHandler(this.CreateNewModule_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvTreningModule)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnMainMenu;
        private System.Windows.Forms.ComboBox cmbTreningModule;
        private System.Windows.Forms.DataGridView dgvTreningModule;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtNewModuleName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtEndDate;
        private System.Windows.Forms.Button btnCreateNewModule;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtNewTestName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cmbModuleRewrite;
        private System.Windows.Forms.TextBox txtModuleNameRewrite;
        private System.Windows.Forms.TextBox txtStartModuleRewrite;
        private System.Windows.Forms.TextBox txtEndModuleRewrite;
        private System.Windows.Forms.ComboBox cmbTreningIDModuleRewrite;
        private System.Windows.Forms.Button btnRewrite;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
    }
}