﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ITAcademy_DB_Application
{
    public partial class CreateNewModule : Form
    {
        const string _sqlConnectionString = "Server=tcp:itacademy.database.windows.net,1433;" +
            "Database=Ravsher;" +
            "User ID=Ravsher;Password=Sduq97223;" +
            "Trusted_Connection=False;Encrypt=True;";
        public CreateNewModule()
        {
            InitializeComponent();
        }

        private void btnMainMenu_Click(object sender, EventArgs e)
        {
            ActiveForm.Hide();
            MainMenu Menu = new MainMenu();
            Menu.ShowDialog();
            Close();
        }


        private void cmbTreningModule_SelectedIndexChanged(object sender, EventArgs e)
        {
            GetTreningModuleInfoFromDataBase();
            GetModulesList();
            
        }
        private void CreateNewModule_Load(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(_sqlConnectionString);
            con.Open();

            GetTreningsList(con);

            con.Close();

        }

        private void GetTreningModuleInfoFromDataBase()
        {
            SqlConnection con = new SqlConnection(_sqlConnectionString);
            con.Open();

            Trening tr = (Trening)cmbTreningModule.SelectedItem;
            SqlDataAdapter DA = new SqlDataAdapter($"SELECT M.Module_Name, T.Test_Name, M.Start_Data, M.End_Data FROM [dbo].[Trening] TR INNER JOIN[dbo].[Module] M ON TR.ID = M.TreningID INNER JOIN [dbo].[Test] T ON M.ID = T.ModuleID WHERE TR.ID = {tr.id}", con);

            DataSet DS = new DataSet();
            DA.Fill(DS, "TM");

            dgvTreningModule.DataSource = DS.Tables["TM"];
            dgvTreningModule.Refresh();

            con.Close();
        }

        private void GetTreningsList(SqlConnection con)
        {
            SqlCommand comm = new SqlCommand("select * from [dbo].[Trening]", con);
            SqlDataReader DR = comm.ExecuteReader();

            cmbTreningModule.Items.Clear();
            while (DR.Read())
            {
                Trening tr = new Trening(int.Parse(DR[0].ToString()), DR[1].ToString(), DR[2].ToString(), DR[3].ToString(), int.Parse(DR[4].ToString()));
                cmbTreningModule.Items.Add(tr);
                cmbTreningModule.DisplayMember = "name";
                cmbTreningIDModuleRewrite.Items.Add(tr);
                cmbTreningIDModuleRewrite.DisplayMember = "name";
            }
        }
        private void GetModulesList()
        {
            Trening trnr = (Trening)cmbTreningModule.SelectedItem;
            SqlConnection con = new SqlConnection(_sqlConnectionString);
            con.Open();

            SqlCommand comm = new SqlCommand($"select * from [dbo].[Module] WHERE TreningID = {trnr.id}", con);
            SqlDataReader DR = comm.ExecuteReader();

            cmbModuleRewrite.Items.Clear();
            while (DR.Read())
            {
                Module mdl = new Module(int.Parse(DR[0].ToString()), DR[1].ToString(), DR[2].ToString(), DR[3].ToString(), int.Parse(DR[4].ToString()));
                cmbModuleRewrite.Items.Add(mdl);
                cmbModuleRewrite.DisplayMember = "name";            
            }

            con.Close();
        }

        private void btnCreateNewModule_Click(object sender, EventArgs e)
        {
            Random rnd = new Random();
            SqlConnection con = new SqlConnection(_sqlConnectionString);
            con.Open();

            try
            {
                Trening tr = (Trening)cmbTreningModule.SelectedItem;
                if ((txtNewModuleName.Text == "" || txtNewModuleName.Text.Length > 50) ||
                        (txtNewTestName.Text == "" || txtNewTestName.Text.Length > 50) ||
                        (txtEndDate.Text == ""))
                {
                    throw new Exception();
                }
                if ((txtEndDate.Text[4] != '-' || txtEndDate.Text[7] != '-') ||
                        (!Char.IsDigit(txtEndDate.Text[rnd.Next(0, 3)])) || !Char.IsDigit(txtEndDate.Text[rnd.Next(5, 6)]) || !Char.IsDigit(txtEndDate.Text[rnd.Next(8, 9)]))
                {
                    throw new ArgumentException();
                }
                SqlCommand comm = new SqlCommand($"INSERT INTO [dbo].[Module] (Module_Name, Start_Data, End_Data, TreningID) VALUES ('{txtNewModuleName.Text}',CURRENT_TIMESTAMP,'{txtEndDate.Text}',{tr.id})", con);
                comm.ExecuteNonQuery();
                con.Close();

                con.Open();
                SqlCommand comm1 = new SqlCommand($"INSERT INTO [dbo].[Test] (Test_Name, ModuleID) VALUES ('{txtNewTestName.Text}',(SELECT M.ID FROM [dbo].[Module] M WHERE M.Module_Name = '{txtNewModuleName.Text}'))", con);
                comm1.ExecuteNonQuery();
                con.Close();

                txtEndDate.Text = null;
                txtNewModuleName.Text = null;
                txtNewTestName.Text = null;
                GetTreningModuleInfoFromDataBase();
            }
            catch (NullReferenceException)
            {
                MessageBox.Show("Виберіть один з тренінгів");
            }
            catch (ArgumentException)
            {
                MessageBox.Show("Не вірно введена дата\n[YYYY]-[MM]-[DD] Формат!");
            }
            catch (Exception)
            {
                MessageBox.Show("Не коректно введені дані, можливо ви пропустили строку, або ввели занадто довгу назву");
            }

            con.Close();
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void cmbModuleRewrite_SelectedIndexChanged(object sender, EventArgs e)
        {
            Module mdl = (Module)cmbModuleRewrite.SelectedItem;
            cmbTreningIDModuleRewrite.SelectedItem = cmbTreningModule.SelectedItem;
            txtModuleNameRewrite.Text = mdl.name;
            txtStartModuleRewrite.Text = mdl.startTime;
            txtEndModuleRewrite.Text = mdl.endTime;
        }

        private void btnRewrite_Click(object sender, EventArgs e)
        {
            Random rnd = new Random();
            Module mdl = (Module)cmbModuleRewrite.SelectedItem;
            Trening trnr = (Trening)cmbTreningIDModuleRewrite.SelectedItem;
            SqlConnection con = new SqlConnection(_sqlConnectionString);
            con.Open();

            try
            {
                if ((txtModuleNameRewrite.Text == "" || txtModuleNameRewrite.Text.Length > 50) ||
                    (txtStartModuleRewrite.Text == "" || txtEndModuleRewrite.Text == ""))
                {
                    throw new Exception();
                }
                con.Close();

                con.Open();
                SqlCommand comm = new SqlCommand($"UPDATE Module SET Module_Name = '{txtModuleNameRewrite.Text}', Start_Data = '{txtStartModuleRewrite.Text}', End_Data = '{txtEndModuleRewrite.Text}', TreningID =  {trnr.id} WHERE ID = {mdl.id}",con);
                comm.ExecuteNonQuery();

                GetTreningModuleInfoFromDataBase();
                txtEndModuleRewrite.Text = null;
                txtModuleNameRewrite.Text = null;
                txtStartModuleRewrite.Text = null;
                cmbTreningIDModuleRewrite.SelectedItem = null;
            }
            catch (NullReferenceException)
            {
                MessageBox.Show("Виберіть один з модулів");
            }
            catch (Exception)
            {
                MessageBox.Show("Не коректно введені дані, можливо ви пропустили строку, або ввели занадто довгу назву");
            }
            con.Close();
        }

        private void label8_Click(object sender, EventArgs e)
        {

        }
    }
}
