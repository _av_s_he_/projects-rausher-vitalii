﻿namespace ITAcademy_DB_Application
{
    partial class CreateNewTrening
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnMainMenu = new System.Windows.Forms.Button();
            this.dgvTrenerTrening = new System.Windows.Forms.DataGridView();
            this.cmbTrenerCreator = new System.Windows.Forms.ComboBox();
            this.btnCreateNewModule = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtTreningName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtModuleName = new System.Windows.Forms.TextBox();
            this.btnCreateNewTrening = new System.Windows.Forms.Button();
            this.dgvNewTreningModuleTestQuestion = new System.Windows.Forms.DataGridView();
            this.label3 = new System.Windows.Forms.Label();
            this.txtQuestion = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtAnswer1 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtAnswer2 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtAnswer3 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtRightAnswer = new System.Windows.Forms.TextBox();
            this.btnCreateNewQuestion = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.txtTestName = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txtNewTrenerName = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtNewTrenerBD = new System.Windows.Forms.TextBox();
            this.btnTrenerCreate = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.txtEndTreningDate = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtEndModuleDate = new System.Windows.Forms.TextBox();
            this.cmbTestId = new System.Windows.Forms.ComboBox();
            this.btnTreningRewrite = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTrenerTrening)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvNewTreningModuleTestQuestion)).BeginInit();
            this.SuspendLayout();
            // 
            // btnMainMenu
            // 
            this.btnMainMenu.BackColor = System.Drawing.SystemColors.Desktop;
            this.btnMainMenu.ForeColor = System.Drawing.Color.Lime;
            this.btnMainMenu.Location = new System.Drawing.Point(12, 12);
            this.btnMainMenu.Name = "btnMainMenu";
            this.btnMainMenu.Size = new System.Drawing.Size(119, 63);
            this.btnMainMenu.TabIndex = 0;
            this.btnMainMenu.Text = "<-- Назад на головне меню";
            this.btnMainMenu.UseVisualStyleBackColor = false;
            this.btnMainMenu.Click += new System.EventHandler(this.btnMainMenu_Click);
            // 
            // dgvTrenerTrening
            // 
            this.dgvTrenerTrening.BackgroundColor = System.Drawing.SystemColors.Desktop;
            this.dgvTrenerTrening.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTrenerTrening.Location = new System.Drawing.Point(41, 169);
            this.dgvTrenerTrening.Name = "dgvTrenerTrening";
            this.dgvTrenerTrening.Size = new System.Drawing.Size(516, 241);
            this.dgvTrenerTrening.TabIndex = 1;
            this.dgvTrenerTrening.Text = "dataGridView1";
            // 
            // cmbTrenerCreator
            // 
            this.cmbTrenerCreator.BackColor = System.Drawing.SystemColors.Desktop;
            this.cmbTrenerCreator.ForeColor = System.Drawing.Color.Lime;
            this.cmbTrenerCreator.FormattingEnabled = true;
            this.cmbTrenerCreator.Location = new System.Drawing.Point(41, 127);
            this.cmbTrenerCreator.Name = "cmbTrenerCreator";
            this.cmbTrenerCreator.Size = new System.Drawing.Size(384, 23);
            this.cmbTrenerCreator.TabIndex = 2;
            this.cmbTrenerCreator.Text = "Виберіть тренера для створення тренінгу";
            this.cmbTrenerCreator.SelectedIndexChanged += new System.EventHandler(this.cmbTrenerCreator_SelectedIndexChanged);
            // 
            // btnCreateNewModule
            // 
            this.btnCreateNewModule.BackColor = System.Drawing.SystemColors.Desktop;
            this.btnCreateNewModule.ForeColor = System.Drawing.Color.Lime;
            this.btnCreateNewModule.Location = new System.Drawing.Point(1162, 12);
            this.btnCreateNewModule.Name = "btnCreateNewModule";
            this.btnCreateNewModule.Size = new System.Drawing.Size(118, 62);
            this.btnCreateNewModule.TabIndex = 3;
            this.btnCreateNewModule.Text = "Створити новий модуль -->";
            this.btnCreateNewModule.UseVisualStyleBackColor = false;
            this.btnCreateNewModule.Click += new System.EventHandler(this.btnCreateNewModule_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Lime;
            this.label1.Location = new System.Drawing.Point(587, 122);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(170, 15);
            this.label1.TabIndex = 4;
            this.label1.Text = "Введіть назву нового тренінгу";
            // 
            // txtTreningName
            // 
            this.txtTreningName.BackColor = System.Drawing.SystemColors.Desktop;
            this.txtTreningName.ForeColor = System.Drawing.Color.Lime;
            this.txtTreningName.Location = new System.Drawing.Point(587, 140);
            this.txtTreningName.Name = "txtTreningName";
            this.txtTreningName.Size = new System.Drawing.Size(374, 23);
            this.txtTreningName.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Lime;
            this.label2.Location = new System.Drawing.Point(587, 170);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(374, 15);
            this.label2.TabIndex = 6;
            this.label2.Text = "Введіть назву обов\'язкового першого модулю для даного тренінгу";
            // 
            // txtModuleName
            // 
            this.txtModuleName.BackColor = System.Drawing.SystemColors.Desktop;
            this.txtModuleName.ForeColor = System.Drawing.Color.Lime;
            this.txtModuleName.Location = new System.Drawing.Point(587, 188);
            this.txtModuleName.Name = "txtModuleName";
            this.txtModuleName.Size = new System.Drawing.Size(374, 23);
            this.txtModuleName.TabIndex = 7;
            // 
            // btnCreateNewTrening
            // 
            this.btnCreateNewTrening.BackColor = System.Drawing.SystemColors.Desktop;
            this.btnCreateNewTrening.ForeColor = System.Drawing.Color.Lime;
            this.btnCreateNewTrening.Location = new System.Drawing.Point(587, 267);
            this.btnCreateNewTrening.Name = "btnCreateNewTrening";
            this.btnCreateNewTrening.Size = new System.Drawing.Size(201, 91);
            this.btnCreateNewTrening.TabIndex = 8;
            this.btnCreateNewTrening.Text = "Створити новий тренінг";
            this.btnCreateNewTrening.UseVisualStyleBackColor = false;
            this.btnCreateNewTrening.Click += new System.EventHandler(this.btnCreateNewTrening_Click);
            // 
            // dgvNewTreningModuleTestQuestion
            // 
            this.dgvNewTreningModuleTestQuestion.BackgroundColor = System.Drawing.SystemColors.Desktop;
            this.dgvNewTreningModuleTestQuestion.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvNewTreningModuleTestQuestion.Location = new System.Drawing.Point(659, 398);
            this.dgvNewTreningModuleTestQuestion.Name = "dgvNewTreningModuleTestQuestion";
            this.dgvNewTreningModuleTestQuestion.Size = new System.Drawing.Size(621, 290);
            this.dgvNewTreningModuleTestQuestion.TabIndex = 9;
            this.dgvNewTreningModuleTestQuestion.Text = "dataGridView2";
            this.dgvNewTreningModuleTestQuestion.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvNewTreningModuleTestQuestion_CellContentClick);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.Lime;
            this.label3.Location = new System.Drawing.Point(41, 430);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(248, 15);
            this.label3.TabIndex = 10;
            this.label3.Text = "Питання для тесту новоствореного модулю";
            // 
            // txtQuestion
            // 
            this.txtQuestion.BackColor = System.Drawing.SystemColors.Desktop;
            this.txtQuestion.ForeColor = System.Drawing.Color.Lime;
            this.txtQuestion.Location = new System.Drawing.Point(41, 449);
            this.txtQuestion.Name = "txtQuestion";
            this.txtQuestion.Size = new System.Drawing.Size(384, 23);
            this.txtQuestion.TabIndex = 11;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.Lime;
            this.label4.Location = new System.Drawing.Point(41, 479);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(120, 15);
            this.label4.TabIndex = 12;
            this.label4.Text = "Варіант відповіді №1";
            // 
            // txtAnswer1
            // 
            this.txtAnswer1.BackColor = System.Drawing.SystemColors.Desktop;
            this.txtAnswer1.ForeColor = System.Drawing.Color.Lime;
            this.txtAnswer1.Location = new System.Drawing.Point(41, 498);
            this.txtAnswer1.Name = "txtAnswer1";
            this.txtAnswer1.Size = new System.Drawing.Size(384, 23);
            this.txtAnswer1.TabIndex = 13;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.Lime;
            this.label5.Location = new System.Drawing.Point(41, 528);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(120, 15);
            this.label5.TabIndex = 14;
            this.label5.Text = "Варіант відповіді №2";
            // 
            // txtAnswer2
            // 
            this.txtAnswer2.BackColor = System.Drawing.SystemColors.Desktop;
            this.txtAnswer2.ForeColor = System.Drawing.Color.Lime;
            this.txtAnswer2.Location = new System.Drawing.Point(41, 547);
            this.txtAnswer2.Name = "txtAnswer2";
            this.txtAnswer2.Size = new System.Drawing.Size(384, 23);
            this.txtAnswer2.TabIndex = 15;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.Color.Lime;
            this.label6.Location = new System.Drawing.Point(41, 577);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(120, 15);
            this.label6.TabIndex = 16;
            this.label6.Text = "Варіант відповіді №3";
            // 
            // txtAnswer3
            // 
            this.txtAnswer3.BackColor = System.Drawing.SystemColors.Desktop;
            this.txtAnswer3.ForeColor = System.Drawing.Color.Lime;
            this.txtAnswer3.Location = new System.Drawing.Point(41, 596);
            this.txtAnswer3.Name = "txtAnswer3";
            this.txtAnswer3.Size = new System.Drawing.Size(384, 23);
            this.txtAnswer3.TabIndex = 17;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.Color.Lime;
            this.label7.Location = new System.Drawing.Point(41, 626);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(169, 15);
            this.label7.TabIndex = 18;
            this.label7.Text = "Правильний варіант відповіді";
            // 
            // txtRightAnswer
            // 
            this.txtRightAnswer.BackColor = System.Drawing.SystemColors.Desktop;
            this.txtRightAnswer.ForeColor = System.Drawing.Color.Lime;
            this.txtRightAnswer.Location = new System.Drawing.Point(41, 645);
            this.txtRightAnswer.Name = "txtRightAnswer";
            this.txtRightAnswer.Size = new System.Drawing.Size(384, 23);
            this.txtRightAnswer.TabIndex = 19;
            // 
            // btnCreateNewQuestion
            // 
            this.btnCreateNewQuestion.BackColor = System.Drawing.SystemColors.Desktop;
            this.btnCreateNewQuestion.ForeColor = System.Drawing.Color.Lime;
            this.btnCreateNewQuestion.Location = new System.Drawing.Point(490, 461);
            this.btnCreateNewQuestion.Name = "btnCreateNewQuestion";
            this.btnCreateNewQuestion.Size = new System.Drawing.Size(142, 95);
            this.btnCreateNewQuestion.TabIndex = 20;
            this.btnCreateNewQuestion.Text = "Додати питання до тесту модулю";
            this.btnCreateNewQuestion.UseVisualStyleBackColor = false;
            this.btnCreateNewQuestion.Click += new System.EventHandler(this.btnCreateNewQuestion_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.Color.Lime;
            this.label8.Location = new System.Drawing.Point(587, 218);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(367, 15);
            this.label8.TabIndex = 21;
            this.label8.Text = "Введіть назву обо\'язкового тесту для модуля створеного тренінгу";
            // 
            // txtTestName
            // 
            this.txtTestName.BackColor = System.Drawing.SystemColors.Desktop;
            this.txtTestName.ForeColor = System.Drawing.Color.Lime;
            this.txtTestName.Location = new System.Drawing.Point(587, 237);
            this.txtTestName.Name = "txtTestName";
            this.txtTestName.Size = new System.Drawing.Size(374, 23);
            this.txtTestName.TabIndex = 22;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label9.ForeColor = System.Drawing.Color.Lime;
            this.label9.Location = new System.Drawing.Point(587, 378);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(504, 15);
            this.label9.TabIndex = 23;
            this.label9.Text = "УВАГА! Питання до тесту додавати тільки після створення тренінгу, наступним кроко" +
    "м";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label10.ForeColor = System.Drawing.Color.Lime;
            this.label10.Location = new System.Drawing.Point(993, 151);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(253, 25);
            this.label10.TabIndex = 24;
            this.label10.Text = "Створити нового тренера";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.ForeColor = System.Drawing.Color.Lime;
            this.label11.Location = new System.Drawing.Point(993, 196);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(150, 15);
            this.label11.TabIndex = 26;
            this.label11.Text = "Прізвище Ім\'я Побатькові";
            // 
            // txtNewTrenerName
            // 
            this.txtNewTrenerName.BackColor = System.Drawing.SystemColors.Desktop;
            this.txtNewTrenerName.ForeColor = System.Drawing.Color.Lime;
            this.txtNewTrenerName.Location = new System.Drawing.Point(993, 215);
            this.txtNewTrenerName.Name = "txtNewTrenerName";
            this.txtNewTrenerName.Size = new System.Drawing.Size(217, 23);
            this.txtNewTrenerName.TabIndex = 27;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.ForeColor = System.Drawing.Color.Lime;
            this.label12.Location = new System.Drawing.Point(993, 245);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(103, 15);
            this.label12.TabIndex = 28;
            this.label12.Text = "Дата народження";
            // 
            // txtNewTrenerBD
            // 
            this.txtNewTrenerBD.BackColor = System.Drawing.SystemColors.Desktop;
            this.txtNewTrenerBD.ForeColor = System.Drawing.Color.Lime;
            this.txtNewTrenerBD.Location = new System.Drawing.Point(993, 264);
            this.txtNewTrenerBD.Name = "txtNewTrenerBD";
            this.txtNewTrenerBD.Size = new System.Drawing.Size(217, 23);
            this.txtNewTrenerBD.TabIndex = 29;
            // 
            // btnTrenerCreate
            // 
            this.btnTrenerCreate.BackColor = System.Drawing.SystemColors.Desktop;
            this.btnTrenerCreate.ForeColor = System.Drawing.Color.Lime;
            this.btnTrenerCreate.Location = new System.Drawing.Point(993, 298);
            this.btnTrenerCreate.Name = "btnTrenerCreate";
            this.btnTrenerCreate.Size = new System.Drawing.Size(217, 50);
            this.btnTrenerCreate.TabIndex = 30;
            this.btnTrenerCreate.Text = "Створити нового тренера";
            this.btnTrenerCreate.UseVisualStyleBackColor = false;
            this.btnTrenerCreate.Click += new System.EventHandler(this.btnTrenerCreate_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.ForeColor = System.Drawing.Color.Lime;
            this.label13.Location = new System.Drawing.Point(794, 267);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(151, 15);
            this.label13.TabIndex = 31;
            this.label13.Text = "Дата завершення тренінгу";
            this.label13.Click += new System.EventHandler(this.label13_Click);
            // 
            // txtEndTreningDate
            // 
            this.txtEndTreningDate.BackColor = System.Drawing.SystemColors.Desktop;
            this.txtEndTreningDate.ForeColor = System.Drawing.Color.Lime;
            this.txtEndTreningDate.Location = new System.Drawing.Point(794, 286);
            this.txtEndTreningDate.Name = "txtEndTreningDate";
            this.txtEndTreningDate.Size = new System.Drawing.Size(167, 23);
            this.txtEndTreningDate.TabIndex = 32;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.ForeColor = System.Drawing.Color.Lime;
            this.label14.Location = new System.Drawing.Point(794, 316);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(150, 15);
            this.label14.TabIndex = 33;
            this.label14.Text = "Дата завершення модулю";
            // 
            // txtEndModuleDate
            // 
            this.txtEndModuleDate.BackColor = System.Drawing.SystemColors.Desktop;
            this.txtEndModuleDate.ForeColor = System.Drawing.Color.Lime;
            this.txtEndModuleDate.Location = new System.Drawing.Point(794, 335);
            this.txtEndModuleDate.Name = "txtEndModuleDate";
            this.txtEndModuleDate.Size = new System.Drawing.Size(167, 23);
            this.txtEndModuleDate.TabIndex = 34;
            // 
            // cmbTestId
            // 
            this.cmbTestId.BackColor = System.Drawing.SystemColors.Desktop;
            this.cmbTestId.ForeColor = System.Drawing.Color.Lime;
            this.cmbTestId.FormattingEnabled = true;
            this.cmbTestId.Location = new System.Drawing.Point(348, 422);
            this.cmbTestId.Name = "cmbTestId";
            this.cmbTestId.Size = new System.Drawing.Size(284, 23);
            this.cmbTestId.TabIndex = 35;
            this.cmbTestId.Text = "Виберіть модуль тесту";
            this.cmbTestId.SelectedIndexChanged += new System.EventHandler(this.cmbTestId_SelectedIndexChanged);
            // 
            // btnTreningRewrite
            // 
            this.btnTreningRewrite.BackColor = System.Drawing.SystemColors.Desktop;
            this.btnTreningRewrite.ForeColor = System.Drawing.Color.Lime;
            this.btnTreningRewrite.Location = new System.Drawing.Point(1162, 627);
            this.btnTreningRewrite.Name = "btnTreningRewrite";
            this.btnTreningRewrite.Size = new System.Drawing.Size(118, 61);
            this.btnTreningRewrite.TabIndex = 36;
            this.btnTreningRewrite.Text = "Редагувати тренінг -->";
            this.btnTreningRewrite.UseVisualStyleBackColor = false;
            this.btnTreningRewrite.Click += new System.EventHandler(this.btnTreningRewrite_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label15.ForeColor = System.Drawing.Color.Lime;
            this.label15.Location = new System.Drawing.Point(162, 13);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(635, 20);
            this.label15.TabIndex = 37;
            this.label15.Text = "Для створення тренінгу потрібно обрати тренера під чиїм іменем його буде створено" +
    "";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label16.ForeColor = System.Drawing.Color.Lime;
            this.label16.Location = new System.Drawing.Point(162, 37);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(547, 20);
            this.label16.TabIndex = 38;
            this.label16.Text = "Для створення нового тренера потрібно ввести дані в відповідні комірки";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label17.ForeColor = System.Drawing.Color.Lime;
            this.label17.Location = new System.Drawing.Point(162, 61);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(936, 17);
            this.label17.TabIndex = 39;
            this.label17.Text = "Для створення нових питань для тесту потрібно обрати тренінг на якому знаходиться" +
    " модуль з даним тестом, після чого вибрати бажаний тест";
            // 
            // CreateNewTrening
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Desktop;
            this.ClientSize = new System.Drawing.Size(1292, 709);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.btnTreningRewrite);
            this.Controls.Add(this.cmbTestId);
            this.Controls.Add(this.txtEndModuleDate);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.txtEndTreningDate);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.btnTrenerCreate);
            this.Controls.Add(this.txtNewTrenerBD);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.txtNewTrenerName);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtTestName);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.btnCreateNewQuestion);
            this.Controls.Add(this.txtRightAnswer);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtAnswer3);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtAnswer2);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtAnswer1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtQuestion);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.dgvNewTreningModuleTestQuestion);
            this.Controls.Add(this.btnCreateNewTrening);
            this.Controls.Add(this.txtModuleName);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtTreningName);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnCreateNewModule);
            this.Controls.Add(this.cmbTrenerCreator);
            this.Controls.Add(this.dgvTrenerTrening);
            this.Controls.Add(this.btnMainMenu);
            this.ForeColor = System.Drawing.SystemColors.Desktop;
            this.Name = "CreateNewTrening";
            this.Text = "CreateNewTrening";
            this.Load += new System.EventHandler(this.CreateNewTrening_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvTrenerTrening)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvNewTreningModuleTestQuestion)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnMainMenu;
        private System.Windows.Forms.DataGridView dgvTrenerTrening;
        private System.Windows.Forms.ComboBox cmbTrenerCreator;
        private System.Windows.Forms.Button btnCreateNewModule;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtTreningName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtModuleName;
        private System.Windows.Forms.Button btnCreateNewTrening;
        private System.Windows.Forms.DataGridView dgvNewTreningModuleTestQuestion;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtQuestion;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtAnswer1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtAnswer2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtAnswer3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtRightAnswer;
        private System.Windows.Forms.Button btnCreateNewQuestion;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtTestName;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txt;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtNewTrenerName;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtNewTrenerBD;
        private System.Windows.Forms.Button btnTrenerCreate;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtEndTreningDate;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtEndModuleDate;
        private System.Windows.Forms.ComboBox cmbTestId;
        private System.Windows.Forms.Button btnTreningRewrite;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
    }
}