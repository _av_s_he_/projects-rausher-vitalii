﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ITAcademy_DB_Application
{
    public partial class cmbAddTreningForUser : Form
    {
        const string _sqlConnectionString = "Server=tcp:itacademy.database.windows.net,1433;" +
            "Database=Ravsher;" +
            "User ID=Ravsher;Password=Sduq97223;" +
            "Trusted_Connection=False;Encrypt=True;";
        public cmbAddTreningForUser()
        {
            InitializeComponent();
        }

        private void dgvUserTrenings_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
        }

        private void AddTreningForUser_Load(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(_sqlConnectionString);
            con.Open();

            SqlCommand comm = new SqlCommand("select * from [dbo].[User]", con);
            SqlDataReader DR1 = comm.ExecuteReader();

            cmbUser.Items.Clear();
            while (DR1.Read())
            {
                Classes usr = new Classes(int.Parse(DR1[0].ToString()), DR1[1].ToString(), DR1[2].ToString());
                cmbUser.Items.Add(usr);
                cmbUser.DisplayMember = "name";
            }
            con.Close();

            con.Open();
            SqlCommand comm1 = new SqlCommand("select * from [dbo].[Trening]", con);
            SqlDataReader DR2 = comm1.ExecuteReader();

            cmbTreningInfo.Items.Clear();
            while (DR2.Read())
            {
                Trening trng = new Trening(int.Parse(DR2[0].ToString()), DR2[1].ToString(), DR2[2].ToString(), DR2[3].ToString(), int.Parse(DR2[4].ToString()));
                cmbTreningInfo.Items.Add(trng);
                cmbTreningInfo.DisplayMember = "name";
            }
            con.Close();
        }


        private void GetUserTreningInfoFromDataBase()
        {
            SqlConnection con = new SqlConnection(_sqlConnectionString);
            con.Open();

            Classes usr = (Classes)cmbUser.SelectedItem;
            SqlDataAdapter DA = new SqlDataAdapter($"SELECT U.PIB, TR.Trening_Name, T.PIB FROM [dbo].[User_Trening] UTR INNER JOIN [dbo].[User] U ON UTR.UserID = U.ID INNER JOIN [dbo].[Trening] TR ON UTR.TreningID = TR.ID INNER JOIN [dbo].[Trener] T ON TR.TrenerID = T.ID WHERE U.ID = {usr.id}", con);

            DataSet DS = new DataSet();
            DA.Fill(DS, "UTR");

            dgvUserTrenings.DataSource = DS.Tables["UTR"];
            dgvUserTrenings.Refresh();

            con.Close();
        }
        private void GetTreningInfoFromDataBase()
        {
            SqlConnection con = new SqlConnection(_sqlConnectionString);
            con.Open();

            Trening trng = (Trening)cmbTreningInfo.SelectedItem;
            SqlDataAdapter DA = new SqlDataAdapter($"SELECT M.Module_Name, T.Test_Name FROM[dbo].[Trening] TR INNER JOIN[dbo].[Module] M ON TR.ID = M.TreningID INNER JOIN[dbo].[Test] T ON M.ID = T.ModuleID WHERE TR.ID = {trng.id} ORDER BY M.Module_Name, T.Test_Name", con);

            DataSet DS = new DataSet();
            DA.Fill(DS, "TR");

            dgvTreningInfo.DataSource = DS.Tables["TR"];
            dgvTreningInfo.Refresh();

            con.Close();
        }

        private void cmbUser_SelectedIndexChanged(object sender, EventArgs e)
        {
            GetUserTreningInfoFromDataBase();
        }
        private void cmbTreningInfo_SelectedIndexChanged(object sender, EventArgs e)
        {
            GetTreningInfoFromDataBase();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(_sqlConnectionString);
            con.Open();
            try
            {
                Trening trng = (Trening)cmbTreningInfo.SelectedItem;
                Classes usr = (Classes)cmbUser.SelectedItem;

                SqlCommand com = new SqlCommand($"SELECT * FROM User_Trening UTR WHERE UTR.UserID = {usr.id} AND UTR.TreningID = {trng.id}", con);
                SqlDataReader DR = com.ExecuteReader();

                if (DR.Read())
                {
                    throw new Exception();
                }
                else
                {
                    con.Close();

                    con.Open();
                    SqlCommand comm = new SqlCommand($"INSERT INTO [dbo].[User_Trening] (UserID, TreningID) VALUES ({usr.id},{trng.id})", con);
                    comm.ExecuteNonQuery();

                    GetUserTreningInfoFromDataBase();
                }
            }
            catch (NullReferenceException)
            {
                MessageBox.Show("Виберіть користувача та відповідний тренінг на якому користувач ще не записаний");
            }
            catch (Exception)
            {
                MessageBox.Show("Користувач вже записаний на даному тренінгу");
            }
            con.Close();
        }

        private void btnMainMenu_Click(object sender, EventArgs e)
        {
            ActiveForm.Hide();
            MainMenu Menu = new MainMenu();
            Menu.ShowDialog();
            Close();
        }

        private void btnCreateNewTrening_Click(object sender, EventArgs e)
        {
            ActiveForm.Hide();
            CreateNewTrening CNT = new CreateNewTrening();
            CNT.ShowDialog();
            Close();
        }
    }
}
