﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace ITAcademy_DB_Application
{
    public partial class Selects : Form
    {
        const string _sqlConnectionString = "Server=tcp:itacademy.database.windows.net,1433;" +
            "Database=Ravsher;" +
            "User ID=Ravsher;Password=Sduq97223;" +
            "Trusted_Connection=False;Encrypt=True;";
        public Selects()
        {
            InitializeComponent();
        }

        private void Form1_Load_1(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(_sqlConnectionString);
            con.Open();

            SqlCommand comm = new SqlCommand("select * from [dbo].[User]", con);
            SqlDataReader DR = comm.ExecuteReader();

            cmbUsersList.Items.Clear();
            while (DR.Read())
            {
                Classes usr = new Classes(int.Parse(DR[0].ToString()), DR[1].ToString(), DR[2].ToString());
                cmbUsersList.Items.Add(usr);
                cmbUsersList.DisplayMember = "name";
            }

            con.Close();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                Classes usr = (Classes)cmbUsersList.SelectedItem;
                if (usr == null)
                {
                    throw new NullReferenceException();
                }
                GetInfoFromDataBase($"SELECT U.PIB, M.Module_Name, PS.Mark, PS.Try_Count FROM[dbo].[User] U INNER JOIN[dbo].[Passed_Module] PS ON PS.UserID = U.ID INNER JOIN[dbo].[Module] M ON M.ID = PS.ModuleID WHERE U.ID = {usr.id} ORDER BY PS.Try_Count, M.Module_Name, PS.Mark");
            }
            catch (NullReferenceException)
            {
                MessageBox.Show("Виберіть студента зі стиску");
            }

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            try
            {
                Classes usr = (Classes)cmbUsersList.SelectedItem;
                if (usr == null)
                {
                    throw new NullReferenceException();
                }
                GetInfoFromDataBase($"SELECT U.PIB, TR.Trening_Name, M.Module_Name, T.Test_Name, Q.Question, UA.User_Answer, Q.Right_Answer, UA.Try_Count FROM[dbo].[User] U INNER JOIN[dbo].[User_Answer] UA ON U.ID = UA.UserID INNER JOIN[dbo].[Question] Q ON UA.QuestionID = Q.ID INNER JOIN[dbo].[Test] T ON Q.TestID = T.ID INNER JOIN[dbo].[Module] M ON T.ModuleID = M.ID INNER JOIN[dbo].[Trening] TR ON M.TreningID = TR.ID  WHERE U.ID = { usr.id} ORDER BY UA.Try_Count, TR.Trening_Name, M.Module_Name, T.Test_Name, Q.Question");
            }
            catch (NullReferenceException)
            {
                MessageBox.Show("Виберіть студента зі стиску");
            }
        }

        private void btnGetTreningInfo_Click(object sender, EventArgs e)
        {
            try
            {
                Classes usr = (Classes)cmbUsersList.SelectedItem;
                if (usr == null)
                {
                    throw new NullReferenceException();
                }
                GetInfoFromDataBase($"SELECT U.PIB, TR.Trening_Name, M.Module_Name, T.PIB FROM[dbo].[User] U INNER JOIN[dbo].[User_Trening] UTR ON U.ID = UTR.UserID INNER JOIN[dbo].[Trening] TR ON UTR.TreningID = TR.ID INNER JOIN[dbo].[Trener] T ON TR.TrenerID = T.ID INNER JOIN[dbo].[Module] M ON TR.ID = M.TreningID WHERE U.ID = {usr.id} ORDER BY TR.Trening_Name, M.Module_Name, T.PIB");
            }
            catch (NullReferenceException)
            {
                MessageBox.Show("Виберіть студента зі стиску");
            }
        }

        private void btnGetModuleLinksInfo_Click(object sender, EventArgs e)
        {
            try
            {
                Classes usr = (Classes)cmbUsersList.SelectedItem;
                if (usr == null)
                {
                    throw new NullReferenceException();
                }
                GetInfoFromDataBase($"SELECT M.Module_Name, L.Link FROM[dbo].[User] U INNER JOIN[dbo].[User_Trening] UTR ON U.ID = UTR.UserID INNER JOIN[dbo].[Module] M ON UTR.TreningID = M.TreningID INNER JOIN[dbo].[Link] L ON M.ID = L.ModuleID WHERE U.ID = {usr.id} ORDER BY M.Module_Name, L.Link");
            }
            catch (NullReferenceException)
            {
                MessageBox.Show("Виберіть студента зі стиску");
            }
        }

        private void GetInfoFromDataBase(string condition)
        {
            SqlConnection con = new SqlConnection(_sqlConnectionString);
            con.Open();

            SqlDataAdapter DA = new SqlDataAdapter(condition, con);

            DataSet DS = new DataSet();
            DA.Fill(DS, "DataSet");

            dgvUserInfo.DataSource = DS.Tables["DataSet"];
            dgvUserInfo.Refresh();

            con.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ActiveForm.Hide();
            MainMenu Menu = new MainMenu();
            Menu.ShowDialog();
            Close();
        }

        private void button1_Click_2(object sender, EventArgs e)
        {

            ActiveForm.Hide();
            INSERT MyForm2 = new INSERT();
            MyForm2.ShowDialog();
            Close();
        }

    }
}
