﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ITAcademy_DB_Application
{
    public partial class Lab : Form
    {
        const string _sqlConnectionString = "Server=tcp:itacademy.database.windows.net,1433;" +
            "Database=Ravsher;" +
            "User ID=Ravsher;Password=Sduq97223;" +
            "Trusted_Connection=False;Encrypt=True;";
        public Lab()
        {
            InitializeComponent();
        }

        private void btnMainMenu_Click(object sender, EventArgs e)
        {
            ActiveForm.Hide();
            MainMenu Menu = new MainMenu();
            Menu.ShowDialog();
            Close();
        }

        private void btnSelects_Click(object sender, EventArgs e)
        {
            ActiveForm.Hide();
            Selects Sel = new Selects();
            Sel.ShowDialog();
            Close();
        }

        private void Lab_Load(object sender, EventArgs e)
        {
            GetTrenersList();
            GetUsersList();
        }
        private void GetTrenersList()
        {
            SqlConnection con = new SqlConnection(_sqlConnectionString);
            con.Open();

            SqlCommand comm = new SqlCommand($"SELECT * FROM [dbo].[Trener]", con);
            SqlDataReader DR = comm.ExecuteReader();

            cmbTrener.Items.Clear();
            while (DR.Read())
            {
                Trener trnr = new Trener(int.Parse(DR[0].ToString()), DR[1].ToString(), DR[2].ToString());
                cmbTrener.Items.Add(trnr);
                cmbTrener.DisplayMember = "name";
                cmbUserTrener.Items.Add(trnr);
                cmbUserTrener.DisplayMember = "name";
            }

            con.Close();
        }
        private void GetUsersList()
        {
            SqlConnection con = new SqlConnection(_sqlConnectionString);
            con.Open();

            SqlCommand comm = new SqlCommand($"SELECT * FROM [dbo].[User]", con);
            SqlDataReader DR = comm.ExecuteReader();

            cmbUser.Items.Clear();
            while (DR.Read())
            {
                Classes usr = new Classes(int.Parse(DR[0].ToString()), DR[1].ToString(), DR[2].ToString());
                cmbUser.Items.Add(usr);
                cmbUser.DisplayMember = "name";
            }
            con.Close();
        }

        private void btnSelect1_Click(object sender, EventArgs e)
        {
            try
            {
                Trener trnr = (Trener)cmbTrener.SelectedItem;
                if (trnr == null)
                {
                    throw new Exception();
                }
                GetInfoFromDataBase($"SELECT TR.Trening_Name, M.Module_Name, M.Start_Data, M.End_Data FROM [dbo].[Module] M INNER JOIN [dbo].[Trening] TR ON M.TreningID = TR.ID INNER JOIN [dbo].[Trener] T ON TR.TrenerID = T.ID WHERE T.ID = {trnr.id} ORDER BY TR.Trening_Name, M.Module_Name");
            }
            catch (Exception)
            {
                MessageBox.Show("Виберіть тренера для виконання запиту");
            }
        }

        private void GetInfoFromDataBase(string condition)
        {
            SqlConnection con = new SqlConnection(_sqlConnectionString);
            con.Open();

            SqlDataAdapter DA = new SqlDataAdapter(condition, con);

            DataSet DS = new DataSet();
            DA.Fill(DS, "DataSet");

            dgvAnswer.DataSource = DS.Tables["DataSet"];
            dgvAnswer.Refresh();

            con.Close();
        }

        private void btnSelect2_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtN == null || txtN.Text == "")
                {
                    throw new NullReferenceException();
                }
                if (!int.TryParse(txtN.Text, out _))
                {
                    throw new Exception();
                }
                GetInfoFromDataBase($"SELECT M.Module_Name, PS.Mark, PS.Try_Count, U.PIB, U.BD_Date FROM[dbo].[User] U INNER JOIN[dbo].[Passed_Module] PS ON U.ID = PS.UserID INNER JOIN[dbo].[Module] M ON M.ID = PS.ModuleID WHERE PS.Try_Count = {int.Parse(txtN.Text)}");
            }
            catch (NullReferenceException)
            {
                MessageBox.Show("Введіть кількість спроб для запиту");
            }
            catch (Exception)
            {
                MessageBox.Show($"мдаа...\n{txtN.Text} ж так похоже на число :D");
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Classes usr = (Classes)cmbUser.SelectedItem;
            Trener trnr = (Trener)cmbUserTrener.SelectedItem;
            try
            {
                if (usr == null || trnr == null)
                {
                    throw new NullReferenceException();
                }
                GetInfoFromDataBase($"SELECT TR.Trening_Name, M.Module_Name, Q.Question, UA.User_Answer, Q.Right_Answer, UA.Try_Count FROM [dbo].[Trener] T INNER JOIN [dbo].[Trening] TR ON T.ID = TR.TrenerID INNER JOIN [dbo].[Module] M  ON TR.ID = M.TreningID INNER JOIN [dbo].[Test] TST ON M.ID = TST.ModuleID INNER JOIN [dbo].[Question] Q  ON TST.ID = Q.TestID INNER JOIN [dbo].[User_Answer] UA ON Q.ID = UA.QuestionID INNER JOIN [dbo].[User] U ON U.ID = UA.UserID WHERE T.ID = {trnr.id} AND U.ID = {usr.id} AND UA.User_Answer = Q.Right_Answer");
            }
            catch (NullReferenceException)
            {
                MessageBox.Show("Виберіть користувача та тренера для запиту");
            }
        }
    }
}
