﻿namespace ITAcademy_DB_Application
{
    partial class SetModuleLinks
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmbModule = new System.Windows.Forms.ComboBox();
            this.txtModuleLink = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnSetModuleLink = new System.Windows.Forms.Button();
            this.dgvModuleLinks = new System.Windows.Forms.DataGridView();
            this.btnMainMenu = new System.Windows.Forms.Button();
            this.btnCreateNewModule = new System.Windows.Forms.Button();
            this.cmbLinkID = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtLinkRewrite = new System.Windows.Forms.TextBox();
            this.btnRewriteLink = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvModuleLinks)).BeginInit();
            this.SuspendLayout();
            // 
            // cmbModule
            // 
            this.cmbModule.BackColor = System.Drawing.SystemColors.Desktop;
            this.cmbModule.ForeColor = System.Drawing.Color.Lime;
            this.cmbModule.FormattingEnabled = true;
            this.cmbModule.Location = new System.Drawing.Point(185, 114);
            this.cmbModule.Name = "cmbModule";
            this.cmbModule.Size = new System.Drawing.Size(559, 23);
            this.cmbModule.TabIndex = 0;
            this.cmbModule.Text = "Виберіть модуль";
            this.cmbModule.SelectedIndexChanged += new System.EventHandler(this.cmbModule_SelectedIndexChanged);
            // 
            // txtModuleLink
            // 
            this.txtModuleLink.BackColor = System.Drawing.SystemColors.Desktop;
            this.txtModuleLink.ForeColor = System.Drawing.Color.Lime;
            this.txtModuleLink.Location = new System.Drawing.Point(185, 205);
            this.txtModuleLink.Name = "txtModuleLink";
            this.txtModuleLink.Size = new System.Drawing.Size(343, 23);
            this.txtModuleLink.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Lime;
            this.label1.Location = new System.Drawing.Point(185, 168);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(343, 15);
            this.label1.TabIndex = 2;
            this.label1.Text = "Введіть посилання на матеріали для вибраного вами модуля";
            // 
            // btnSetModuleLink
            // 
            this.btnSetModuleLink.BackColor = System.Drawing.SystemColors.Desktop;
            this.btnSetModuleLink.ForeColor = System.Drawing.Color.Lime;
            this.btnSetModuleLink.Location = new System.Drawing.Point(599, 168);
            this.btnSetModuleLink.Name = "btnSetModuleLink";
            this.btnSetModuleLink.Size = new System.Drawing.Size(145, 60);
            this.btnSetModuleLink.TabIndex = 3;
            this.btnSetModuleLink.Text = "Вставити посилання в даний модуль";
            this.btnSetModuleLink.UseVisualStyleBackColor = false;
            this.btnSetModuleLink.Click += new System.EventHandler(this.btnSetModuleLink_Click);
            // 
            // dgvModuleLinks
            // 
            this.dgvModuleLinks.BackgroundColor = System.Drawing.SystemColors.Desktop;
            this.dgvModuleLinks.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvModuleLinks.GridColor = System.Drawing.SystemColors.Highlight;
            this.dgvModuleLinks.Location = new System.Drawing.Point(185, 286);
            this.dgvModuleLinks.Name = "dgvModuleLinks";
            this.dgvModuleLinks.Size = new System.Drawing.Size(948, 196);
            this.dgvModuleLinks.TabIndex = 4;
            this.dgvModuleLinks.Text = "dataGridView2";
            this.dgvModuleLinks.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvModuleLinks_CellContentClick);
            // 
            // btnMainMenu
            // 
            this.btnMainMenu.BackColor = System.Drawing.SystemColors.Desktop;
            this.btnMainMenu.ForeColor = System.Drawing.Color.Lime;
            this.btnMainMenu.Location = new System.Drawing.Point(12, 12);
            this.btnMainMenu.Name = "btnMainMenu";
            this.btnMainMenu.Size = new System.Drawing.Size(145, 60);
            this.btnMainMenu.TabIndex = 5;
            this.btnMainMenu.Text = "<-- Назад в головне меню";
            this.btnMainMenu.UseVisualStyleBackColor = false;
            this.btnMainMenu.Click += new System.EventHandler(this.btnMainMenu_Click);
            // 
            // btnCreateNewModule
            // 
            this.btnCreateNewModule.BackColor = System.Drawing.SystemColors.Desktop;
            this.btnCreateNewModule.ForeColor = System.Drawing.Color.Lime;
            this.btnCreateNewModule.Location = new System.Drawing.Point(1133, 12);
            this.btnCreateNewModule.Name = "btnCreateNewModule";
            this.btnCreateNewModule.Size = new System.Drawing.Size(145, 60);
            this.btnCreateNewModule.TabIndex = 6;
            this.btnCreateNewModule.Text = "Створити новий модуль -->";
            this.btnCreateNewModule.UseVisualStyleBackColor = false;
            this.btnCreateNewModule.Click += new System.EventHandler(this.btnCreateNewModule_Click);
            // 
            // cmbLinkID
            // 
            this.cmbLinkID.BackColor = System.Drawing.SystemColors.Desktop;
            this.cmbLinkID.ForeColor = System.Drawing.Color.Lime;
            this.cmbLinkID.FormattingEnabled = true;
            this.cmbLinkID.Location = new System.Drawing.Point(185, 510);
            this.cmbLinkID.Name = "cmbLinkID";
            this.cmbLinkID.Size = new System.Drawing.Size(256, 23);
            this.cmbLinkID.TabIndex = 7;
            this.cmbLinkID.Text = "Виберіть посилання для оновлення";
            this.cmbLinkID.SelectedIndexChanged += new System.EventHandler(this.cmbLinkID_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Lime;
            this.label2.Location = new System.Drawing.Point(185, 540);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(141, 15);
            this.label2.TabIndex = 8;
            this.label2.Text = "Відкорегуйте посилання";
            // 
            // txtLinkRewrite
            // 
            this.txtLinkRewrite.BackColor = System.Drawing.SystemColors.Desktop;
            this.txtLinkRewrite.ForeColor = System.Drawing.Color.Lime;
            this.txtLinkRewrite.Location = new System.Drawing.Point(185, 559);
            this.txtLinkRewrite.Name = "txtLinkRewrite";
            this.txtLinkRewrite.Size = new System.Drawing.Size(343, 23);
            this.txtLinkRewrite.TabIndex = 9;
            // 
            // btnRewriteLink
            // 
            this.btnRewriteLink.BackColor = System.Drawing.SystemColors.Desktop;
            this.btnRewriteLink.ForeColor = System.Drawing.Color.Lime;
            this.btnRewriteLink.Location = new System.Drawing.Point(599, 517);
            this.btnRewriteLink.Name = "btnRewriteLink";
            this.btnRewriteLink.Size = new System.Drawing.Size(145, 60);
            this.btnRewriteLink.TabIndex = 10;
            this.btnRewriteLink.Text = "Відкоригувати посилання";
            this.btnRewriteLink.UseVisualStyleBackColor = false;
            this.btnRewriteLink.Click += new System.EventHandler(this.btnRewriteLink_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label3.ForeColor = System.Drawing.Color.Lime;
            this.label3.Location = new System.Drawing.Point(185, 13);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(624, 21);
            this.label3.TabIndex = 11;
            this.label3.Text = "Для додавання посилання до модулю виберіть модуль на введіть посилання";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label4.ForeColor = System.Drawing.Color.Lime;
            this.label4.Location = new System.Drawing.Point(185, 38);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(891, 20);
            this.label4.TabIndex = 12;
            this.label4.Text = "Для редагування посилання оберіть модуль на якому воно знаходиться а потім оберіт" +
    "ь саме посилання та відредагуйте";
            // 
            // SetModuleLinks
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Desktop;
            this.ClientSize = new System.Drawing.Size(1290, 704);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnRewriteLink);
            this.Controls.Add(this.txtLinkRewrite);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cmbLinkID);
            this.Controls.Add(this.btnCreateNewModule);
            this.Controls.Add(this.btnMainMenu);
            this.Controls.Add(this.dgvModuleLinks);
            this.Controls.Add(this.btnSetModuleLink);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtModuleLink);
            this.Controls.Add(this.cmbModule);
            this.ForeColor = System.Drawing.SystemColors.InfoText;
            this.Name = "SetModuleLinks";
            this.Text = "SetModuleLinks";
            this.Load += new System.EventHandler(this.SetModuleLinks_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvModuleLinks)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cmbModule;
        private System.Windows.Forms.TextBox txtModuleLink;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnSetModuleLink;
        private System.Windows.Forms.DataGridView dgvModuleLinks;
        private System.Windows.Forms.Button btnMainMenu;
        private System.Windows.Forms.Button btnCreateNewModule;
        private System.Windows.Forms.ComboBox cmbLinkID;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtLinkRewrite;
        private System.Windows.Forms.Button btnRewriteLink;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
    }
}