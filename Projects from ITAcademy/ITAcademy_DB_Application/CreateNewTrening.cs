﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ITAcademy_DB_Application
{
    public partial class CreateNewTrening : Form
    {
        const string _sqlConnectionString = "Server=tcp:itacademy.database.windows.net,1433;" +
            "Database=Ravsher;" +
            "User ID=Ravsher;Password=Sduq97223;" +
            "Trusted_Connection=False;Encrypt=True;";
        public CreateNewTrening()
        {
            InitializeComponent();
        }

        private void btnMainMenu_Click(object sender, EventArgs e)
        {
            ActiveForm.Hide();
            MainMenu Menu = new MainMenu();
            Menu.ShowDialog();
            Close();
        }

        private void CreateNewTrening_Load(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(_sqlConnectionString);
            con.Open();
            GetTrenersList(con);
            con.Close();

        }
        private void GetTrenersList(SqlConnection con)
        {
            SqlCommand comm = new SqlCommand("select * from [dbo].[Trener]", con);
            SqlDataReader DR = comm.ExecuteReader();

            cmbTrenerCreator.Items.Clear();
            while (DR.Read())
            {
                Trener trnr = new Trener(int.Parse(DR[0].ToString()), DR[1].ToString(), DR[2].ToString());
                cmbTrenerCreator.Items.Add(trnr);
                cmbTrenerCreator.DisplayMember = "name";
            }
        }
        private void cmbTrenerCreator_SelectedIndexChanged(object sender, EventArgs e)
        {
            GetTrenerTreningInfoFromDataBase();

            SqlConnection con = new SqlConnection(_sqlConnectionString);
            con.Open();
            GetTestList(con);
            con.Close();

        }

        private void GetTrenerTreningInfoFromDataBase()
        {
            SqlConnection con = new SqlConnection(_sqlConnectionString);
            con.Open();

            Trener trnr = (Trener)cmbTrenerCreator.SelectedItem;
            SqlDataAdapter DA = new SqlDataAdapter($"SELECT TR.Trening_Name, M.Module_Name, TST.Test_Name, M.Start_Data, M.End_Data FROM[dbo].[Trener] T INNER JOIN[dbo].[Trening] TR ON T.ID = TR.TrenerID INNER JOIN[dbo].[Module] M ON TR.ID = M.TreningID INNER JOIN[dbo].[Test] TST ON M.ID = TST.ModuleID WHERE T.ID = {trnr.id} ORDER BY TR.Trening_Name, M.Module_Name", con);

            DataSet DS = new DataSet();
            DA.Fill(DS, "TT");

            dgvTrenerTrening.DataSource = DS.Tables["TT"];
            dgvTrenerTrening.Refresh();

            con.Close();
        }

        private void btnCreateNewTrening_Click(object sender, EventArgs e)
        {
            Random rnd = new Random();
            SqlConnection con = new SqlConnection(_sqlConnectionString);
            con.Open();

            try
            {
                Trener trnr = (Trener)cmbTrenerCreator.SelectedItem;
                if ((txtTreningName.Text == "" || txtTreningName.Text.Length > 50) ||
                    (txtModuleName.Text == "" || txtModuleName.Text.Length > 50) ||
                    (txtTestName.Text == "" || txtTestName.Text.Length > 50) ||
                    (txtEndTreningDate.Text == "" || txtEndModuleDate.Text == ""))
                {
                    throw new Exception();
                }
                if ((txtEndTreningDate.Text[4] != '-' || txtEndTreningDate.Text[7] != '-') ||
                        (txtEndModuleDate.Text[4] != '-' || txtEndModuleDate.Text[7] != '-') ||
                        (!Char.IsDigit(txtEndModuleDate.Text[rnd.Next(0, 3)])) || !Char.IsDigit(txtEndModuleDate.Text[rnd.Next(5,6)]) || !Char.IsDigit(txtEndModuleDate.Text[rnd.Next(8, 9)]) ||
                        (!Char.IsDigit(txtEndTreningDate.Text[rnd.Next(0, 3)])) || !Char.IsDigit(txtEndTreningDate.Text[rnd.Next(5, 6)]) || !Char.IsDigit(txtEndTreningDate.Text[rnd.Next(8, 9)]))
                {
                    throw new ArgumentException("Не вірно введена дата");
                }
                con.Close();

                con.Open();
                SqlCommand comm = new SqlCommand($"INSERT INTO[dbo].[Trening](Trening_Name, TrenerID, Start_Data, End_Data) VALUES('{txtTreningName.Text}', {trnr.id}, CURRENT_TIMESTAMP, '{txtEndTreningDate.Text}')", con);

                SqlCommand comm1 = new SqlCommand($"INSERT INTO[dbo].[Module](Module_Name, TreningID, Start_Data, End_Data) VALUES('{txtModuleName.Text}', (SELECT TR.ID FROM [dbo].[Trening] TR WHERE TR.Trening_Name = '{txtTreningName.Text}'), CURRENT_TIMESTAMP, '{txtEndModuleDate.Text}')", con);

                SqlCommand comm2 = new SqlCommand($"INSERT INTO [dbo].[Test] (Test_Name, ModuleID) VALUES ('{txtTestName.Text}', (SELECT M.ID FROM [dbo].[Module] M WHERE M.Module_Name = '{txtModuleName.Text}'))", con);
                con.Close();

                con.Open();
                comm.ExecuteNonQuery();
                comm1.ExecuteNonQuery();
                comm2.ExecuteNonQuery();
                con.Close();

                GetTrenerTreningInfoFromDataBase();

                con.Open();
                GetTestList(con);
                con.Close();
                txtTestName.Text = null;
                txtTreningName.Text = null;
                txtModuleName.Text = null;
                txtEndModuleDate.Text = null;
                txtEndTreningDate.Text = null;
            }
            catch (NullReferenceException)
            {
                MessageBox.Show("Виберіть або створіть і потім виберіть тренера");
            }
            catch (ArgumentException)
            {
                MessageBox.Show("Не вірно введена дата\n[YYYY]-[MM]-[DD] Формат!");
            }
            catch (Exception)
            {
                MessageBox.Show("Не коректно введені дані, можливо ви пропустили строку, або ввели занадто довгу назву");
            }

            con.Close();
        }

        private void label13_Click(object sender, EventArgs e)
        {

        }

        private void btnTrenerCreate_Click(object sender, EventArgs e)
        {
            Random rnd = new Random();
            SqlConnection con = new SqlConnection(_sqlConnectionString);
            con.Open();

            try
            {
                if ((txtNewTrenerName.Text == "" || txtNewTrenerName.Text.Length > 50) || (txtNewTrenerBD.Text == ""))
                {
                    throw new Exception();
                }
                if ((txtNewTrenerBD.Text[4] != '-' || txtNewTrenerBD.Text[7] != '-') ||
                        (!Char.IsDigit(txtNewTrenerBD.Text[rnd.Next(0, 3)])) || !Char.IsDigit(txtNewTrenerBD.Text[rnd.Next(5, 6)]) || !Char.IsDigit(txtNewTrenerBD.Text[rnd.Next(8, 9)]))
                {
                    throw new ArgumentException();
                }
                con.Close();

                con.Open();
                SqlCommand comm = new SqlCommand($"INSERT INTO [dbo].[Trener] (PIB, BD_Date) VALUES ('{txtNewTrenerName.Text}', '{txtNewTrenerBD.Text}')", con);
                comm.ExecuteNonQuery();
                GetTrenersList(con);
                con.Close();

                txtNewTrenerName.Text = null;
                txtNewTrenerBD.Text = null;
            }
            catch (ArgumentException)
            {
                MessageBox.Show("Не вірно введена дата\n[YYYY]-[MM]-[DD] Формат!");
            }
            catch (Exception)
            {
                MessageBox.Show("Не коректно введені дані, можливо ви пропустили строку, або ввели занадто довгу назву");
            }

            con.Close();
        }

        private void btnCreateNewQuestion_Click(object sender, EventArgs e)
        {
            Test tst = (Test)cmbTestId.SelectedItem;
            SqlConnection con = new SqlConnection(_sqlConnectionString);
            con.Open();

            try
            {
                if ((txtQuestion.Text == "" || txtQuestion.Text.Length > 300) ||
                    (txtAnswer1.Text == "" || txtAnswer1.Text.Length > 200) ||
                    (txtAnswer2.Text == "" || txtAnswer2.Text.Length > 200) ||
                    (txtAnswer3.Text == "" || txtAnswer3.Text.Length > 200) ||
                    (txtRightAnswer.Text == "" || txtRightAnswer.Text.Length > 200))
                {
                    throw new Exception();
                }
                SqlCommand comm = new SqlCommand($"INSERT INTO[dbo].[Question](Question, Answer1, Answer2, Answer3, Right_Answer, TestID) VALUES('{txtQuestion.Text}', '{txtAnswer1.Text}', '{txtAnswer2.Text}', '{txtAnswer3.Text}', '{txtRightAnswer.Text}', {tst.id})",con);
                comm.ExecuteNonQuery();

                GetTestQuestionInfoFromDataBase();
                con.Close();
                txtQuestion.Text = null;
                txtAnswer1.Text = null;
                txtAnswer2.Text = null;
                txtAnswer3.Text = null;
                txtRightAnswer.Text = null;

            }
            catch (NullReferenceException)
            {
                MessageBox.Show("Виберіть тест");
            }
            catch (Exception)
            {
                MessageBox.Show("Не коректно введені дані, можливо ви пропустили строку, або ввели занадто довгу назву");
            }

            con.Close();
        }

        private void cmbTestId_SelectedIndexChanged(object sender, EventArgs e)
        {
            GetTestQuestionInfoFromDataBase();
        }

        private void GetTestList(SqlConnection con)
        {
            Trener trnr = (Trener)cmbTrenerCreator.SelectedItem;
            SqlCommand comm = new SqlCommand($"SELECT TST.* FROM [dbo].[Test] TST INNER JOIN [dbo].[Module] M ON TST.ModuleID = M.ID INNER JOIN [dbo].[Trening] TR ON M.TreningID = TR.ID INNER JOIN [dbo].[Trener] T ON TR.TrenerID = T.ID WHERE T.ID = {trnr.id}", con);
            SqlDataReader DR = comm.ExecuteReader();

            cmbTestId.Items.Clear();
            while (DR.Read())
            {
                Test tst = new Test(int.Parse(DR[0].ToString()), DR[1].ToString(), int.Parse(DR[2].ToString()));
                cmbTestId.Items.Add(tst);
                cmbTestId.DisplayMember = "name";
            }
        }

        private void dgvNewTreningModuleTestQuestion_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
        }

        private void GetTestQuestionInfoFromDataBase()
        {
            SqlConnection con = new SqlConnection(_sqlConnectionString);
            con.Open();

            Test tst = (Test)cmbTestId.SelectedItem;
            SqlDataAdapter DA = new SqlDataAdapter($"SELECT Q.Question, Q.Answer1, Q.Answer2, Q.Answer3, Q.Right_Answer  FROM [dbo].[Question] Q WHERE Q.TestID = {tst.id}", con);

            DataSet DS = new DataSet();
            DA.Fill(DS, "TMQ");

            dgvNewTreningModuleTestQuestion.DataSource = DS.Tables["TMQ"];
            dgvNewTreningModuleTestQuestion.Refresh();

            con.Close();
        }

        private void btnCreateNewModule_Click(object sender, EventArgs e)
        {
            ActiveForm.Hide();
            CreateNewModule CNM = new CreateNewModule();
            CNM.ShowDialog();
            Close();
        }

        private void btnTreningRewrite_Click(object sender, EventArgs e)
        {
            ActiveForm.Hide();
            RewriteTrening RTR = new RewriteTrening();
            RTR.ShowDialog();
            Close();
        }
    }
}
