﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ITAcademy_DB_Application
{
    public partial class INSERT : Form
    {
        public INSERT()
        {
            InitializeComponent();
        }

        private void FirstTableFilling_Click(object sender, EventArgs e)
        {
            ActiveForm.Hide();
            SetModuleLinks SML = new SetModuleLinks();
            SML.ShowDialog();
            Close();
        }

        private void Form2_Load(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            ActiveForm.Hide();
            cmbAddTreningForUser ATRU = new cmbAddTreningForUser();
            ATRU.ShowDialog();
            Close();
        }

        private void btnCreateNewTrening_Click(object sender, EventArgs e)
        {
            ActiveForm.Hide();
            CreateNewTrening CNT = new CreateNewTrening();
            CNT.ShowDialog();
            Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ActiveForm.Hide();
            CreateNewModule CNM = new CreateNewModule();
            CNM.ShowDialog();
            Close();
        }

        private void btnMainMenu_Click(object sender, EventArgs e)
        {
            ActiveForm.Hide();
            MainMenu Menu = new MainMenu();
            Menu.ShowDialog();
            Close();
        }
    }
}
