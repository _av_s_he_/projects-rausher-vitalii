﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ITAcademy_DB_Application
{
    public partial class MainMenu : Form
    {
        public MainMenu()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ActiveForm.Hide();
            Selects MyForm1 = new Selects();
            MyForm1.ShowDialog();
            Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ActiveForm.Hide();
            INSERT MyForm2 = new INSERT();
            MyForm2.ShowDialog();
            Close();
        }

        private void btnZvit_Click(object sender, EventArgs e)
        {
            ActiveForm.Hide();
            Lab Lb = new Lab();
            Lb.ShowDialog();
            Close();
        }
    }
}
