﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ITAcademy_DB_Application
{
    class Classes
    {
        public int id { get; private set; }
        public string name { get; private set; }
        public string birthDay { get; private set; }

        public Classes(int id, string name, string bd)
        {
            this.id = id;
            this.name = name;
            this.birthDay = bd;
        }
    }
    class Module
    {
        public int id { get; private set; }
        public int treningId { get; private set; }
        public string name { get; private set; }
        public string startTime { get; private set; }
        public string endTime { get; private set; }

        public Module(int id, string name, string start, string end, int treningId)
        {
            this.id = id;
            this.name = name;
            this.startTime = start;
            this.endTime = end;
            this.treningId = treningId;
        }
    }
    class Trening
    {
        public int id { get; private set; }
        public int trenerId { get; private set; }
        public string name { get; private set; }
        public string startTime { get; private set; }
        public string endTime { get; private set; }

        public Trening(int id, string name, string start, string end, int trenerId)
        {
            this.id = id;
            this.name = name;
            this.startTime = start;
            this.endTime = end;
            this.trenerId = trenerId;
        }
    }
    class Trener
    {
        public int id { get; private set; }
        public string name { get; private set; }
        public string birthDay { get; private set; }

        public Trener(int id, string name, string bd)
        {
            this.id = id;
            this.name = name;
            this.birthDay = bd;
        }
    }
    class Test
    {
        public int id { get; private set; }
        public string name { get; private set; }
        public int moduleId { get; private set; }

        public Test(int id, string name, int moduleId)
        {
            this.id = id;
            this.name = name;
            this.moduleId = moduleId;
        }
    }
    class Question
    {
        public int id { get; private set; }
        public string question { get; private set; }
        public string answer1 { get; private set; }
        public string answer2 { get; private set; }
        public string answer3 { get; private set; }
        public string rightAnswer { get; private set; }
        public int testId { get; private set; }

        public Question(int id, string question, string answer1, string answer2, string answer3, string rightAnswer, int testId)
        {
            this.id = id;
            this.question = question;
            this.answer1 = answer1;
            this.answer2 = answer2;
            this.answer3 = answer3;
            this.rightAnswer = rightAnswer;
            this.testId = testId;
        }

    }
    class Link
    {
        public int id { get; private set; }
        public string link { get; private set; }
        public Link (int id, string link)
        {
            this.id = id;
            this.link = link;
        }
    }
}
