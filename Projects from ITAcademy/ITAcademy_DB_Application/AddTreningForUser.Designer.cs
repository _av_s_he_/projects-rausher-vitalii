﻿namespace ITAcademy_DB_Application
{
    partial class cmbAddTreningForUser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmbUser = new System.Windows.Forms.ComboBox();
            this.cmbTreningInfo = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.dgvUserTrenings = new System.Windows.Forms.DataGridView();
            this.dgvTreningInfo = new System.Windows.Forms.DataGridView();
            this.btnMainMenu = new System.Windows.Forms.Button();
            this.btnCreateNewTrening = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvUserTrenings)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTreningInfo)).BeginInit();
            this.SuspendLayout();
            // 
            // cmbUser
            // 
            this.cmbUser.BackColor = System.Drawing.SystemColors.Desktop;
            this.cmbUser.ForeColor = System.Drawing.Color.Lime;
            this.cmbUser.FormattingEnabled = true;
            this.cmbUser.Location = new System.Drawing.Point(88, 186);
            this.cmbUser.Name = "cmbUser";
            this.cmbUser.Size = new System.Drawing.Size(350, 23);
            this.cmbUser.TabIndex = 0;
            this.cmbUser.Text = "Виберіть користувача якого бажаєте записати на тренінг";
            this.cmbUser.SelectedIndexChanged += new System.EventHandler(this.cmbUser_SelectedIndexChanged);
            // 
            // cmbTreningInfo
            // 
            this.cmbTreningInfo.BackColor = System.Drawing.SystemColors.Desktop;
            this.cmbTreningInfo.ForeColor = System.Drawing.Color.Lime;
            this.cmbTreningInfo.FormattingEnabled = true;
            this.cmbTreningInfo.Location = new System.Drawing.Point(852, 186);
            this.cmbTreningInfo.Name = "cmbTreningInfo";
            this.cmbTreningInfo.Size = new System.Drawing.Size(340, 23);
            this.cmbTreningInfo.TabIndex = 1;
            this.cmbTreningInfo.Text = "Виберіть тренінг на який бажаєте записати користувача";
            this.cmbTreningInfo.SelectedIndexChanged += new System.EventHandler(this.cmbTreningInfo_SelectedIndexChanged);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.Desktop;
            this.button1.ForeColor = System.Drawing.Color.Lime;
            this.button1.Location = new System.Drawing.Point(549, 176);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(178, 41);
            this.button1.TabIndex = 2;
            this.button1.Text = "Записати користувача на даний тренінг";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // dgvUserTrenings
            // 
            this.dgvUserTrenings.BackgroundColor = System.Drawing.SystemColors.Desktop;
            this.dgvUserTrenings.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvUserTrenings.GridColor = System.Drawing.SystemColors.Highlight;
            this.dgvUserTrenings.Location = new System.Drawing.Point(88, 253);
            this.dgvUserTrenings.Name = "dgvUserTrenings";
            this.dgvUserTrenings.Size = new System.Drawing.Size(543, 391);
            this.dgvUserTrenings.TabIndex = 3;
            this.dgvUserTrenings.Text = "dataGridView1";
            this.dgvUserTrenings.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvUserTrenings_CellContentClick);
            // 
            // dgvTreningInfo
            // 
            this.dgvTreningInfo.BackgroundColor = System.Drawing.SystemColors.Desktop;
            this.dgvTreningInfo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTreningInfo.GridColor = System.Drawing.SystemColors.Highlight;
            this.dgvTreningInfo.Location = new System.Drawing.Point(655, 253);
            this.dgvTreningInfo.Name = "dgvTreningInfo";
            this.dgvTreningInfo.Size = new System.Drawing.Size(537, 391);
            this.dgvTreningInfo.TabIndex = 4;
            this.dgvTreningInfo.Text = "dataGridView1";
            // 
            // btnMainMenu
            // 
            this.btnMainMenu.BackColor = System.Drawing.SystemColors.Desktop;
            this.btnMainMenu.ForeColor = System.Drawing.Color.Lime;
            this.btnMainMenu.Location = new System.Drawing.Point(12, 12);
            this.btnMainMenu.Name = "btnMainMenu";
            this.btnMainMenu.Size = new System.Drawing.Size(126, 67);
            this.btnMainMenu.TabIndex = 5;
            this.btnMainMenu.Text = "<-- Назад в головне меню";
            this.btnMainMenu.UseVisualStyleBackColor = false;
            this.btnMainMenu.Click += new System.EventHandler(this.btnMainMenu_Click);
            // 
            // btnCreateNewTrening
            // 
            this.btnCreateNewTrening.BackColor = System.Drawing.SystemColors.Desktop;
            this.btnCreateNewTrening.ForeColor = System.Drawing.Color.Lime;
            this.btnCreateNewTrening.Location = new System.Drawing.Point(1155, 12);
            this.btnCreateNewTrening.Name = "btnCreateNewTrening";
            this.btnCreateNewTrening.Size = new System.Drawing.Size(126, 67);
            this.btnCreateNewTrening.TabIndex = 6;
            this.btnCreateNewTrening.Text = "Створити новий тренінг -->";
            this.btnCreateNewTrening.UseVisualStyleBackColor = false;
            this.btnCreateNewTrening.Click += new System.EventHandler(this.btnCreateNewTrening_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label1.ForeColor = System.Drawing.Color.Lime;
            this.label1.Location = new System.Drawing.Point(210, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(886, 21);
            this.label1.TabIndex = 7;
            this.label1.Text = "Виберіть користувача та модуль на який ви бажаєте його записати, для підтвердженн" +
    "я дії натисніть на відповідну кнопку";
            // 
            // cmbAddTreningForUser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Desktop;
            this.ClientSize = new System.Drawing.Size(1293, 710);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnCreateNewTrening);
            this.Controls.Add(this.btnMainMenu);
            this.Controls.Add(this.dgvTreningInfo);
            this.Controls.Add(this.dgvUserTrenings);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.cmbTreningInfo);
            this.Controls.Add(this.cmbUser);
            this.ForeColor = System.Drawing.SystemColors.Desktop;
            this.Name = "cmbAddTreningForUser";
            this.Text = "AddTreningForUser";
            this.Load += new System.EventHandler(this.AddTreningForUser_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvUserTrenings)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTreningInfo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cmbUser;
        private System.Windows.Forms.ComboBox cmbTreningInfo;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridView dgvUserTrenings;
        private System.Windows.Forms.DataGridView dgvTreningInfo;
        private System.Windows.Forms.Button btnMainMenu;
        private System.Windows.Forms.Button btnCreateNewTrening;
        private System.Windows.Forms.Label label1;
    }
}