$(document).ready(function(){
    let tableObject;
    $.get("https://data.austintexas.gov/api/views/p9ma-z6y9/rows.json?accessType=DOWNLOAD",function(rawdata){
        let data=rawdata.data;
        for(let i=0; i<data.length; i++) {
            $("#tableContent").append(`
                <tr>
                    <td>` + (i + 1) + `</td>
                    <td>` + data[i][11] + `</td>
                    <td>` + data[i][10] + `</td>
                    <td>` + data[i][8] + `</td>
                    <td>` + data[i][9] + `</td>
                    <td>` + data[i][1] + `</td>
                   <td><a href="#" class="btn edit btn-warning">Edit</a><a href="#" class="btn delete btn-danger">Delete</a></td>
                </tr>
            `);
        }
        tableObject=$('#dataTable').DataTable();
    });
    $(document).on("submit","#inputForm",function(e){
        e.preventDefault();
        tableObject.destroy();
        if($("#actionInput").val()=="add"){
            let lastNum=Number($("#tableContent").children().last().children().first().text());
            $("#tableContent").append(`
                <tr>
                    <td>`+(lastNum+1)+`</td>
                    <td>`+$("#nameInput").val()+`</td>
                    <td>`+$("#attInput").val()+`</td>
                    <td>`+$("#d1Input").val()+`</td>
                    <td>`+$("#d2Input").val()+`</td>
                    <td>`+$("#rowInput").val()+`</td>
                    <td><a href="#" class="btn btn-warning">Edit</a><a href="#" class="btn btn-danger">Delete</a></td>
                </tr>
            `);
        }
        else{
            $("#tableContent").children().eq(Number($("#idInput").val())-1).html(`
                    <td>`+$("#idInput").val()+`</td>
                    <td>`+$("#nameInput").val()+`</td>
                    <td>`+$("#attInput").val()+`</td>
                    <td>`+$("#d1Input").val()+`</td>
                    <td>`+$("#d2Input").val()+`</td>
                    <td>`+$("#rowInput").val()+`</td>
                    <td><a href="#" class="btn btn-warning">Edit</a><a href="#" class="btn btn-danger">Delete</a></td>
            `);
        }


        tableObject=$('#dataTable').DataTable();
        $("#inputForm")[0].reset();
        $("#actionInput").val('add');
    });
    $(document).on("click",".edit",function(e){
        e.preventDefault();
        let btn=$(this);
        $("#nameInput").val(btn.parent().parent().children().eq(1).html());
        $("#attInput").val(btn.parent().parent().children().eq(2).html());
        $("#d1Input").val(btn.parent().parent().children().eq(3).html());
        $("#d2Input").val(btn.parent().parent().children().eq(4).html());
        $("#rowInput").val(btn.parent().parent().children().eq(5).html());
        $("#idInput").val(btn.parent().parent().children().eq(0).html());
        $("#actionInput").val('edit');
    });
    $(document).on("click",".delete",function(e){
        e.preventDefault();
        let btn=$(this);
        btn.parent().parent().nextAll().each(function (index) {
            $(this).children().first().html($(this).children().first().text()-1);
        });
        btn.parent().parent().remove();
    });
});