$(document).ready(function(){
 	$(function() {
		$(window).scroll(function() {
			if($(this).scrollTop() > 20) {
				$('#m4').fadeIn();
			} else {
				$('#m4').fadeOut();
			}
		});

	$('#m4').click(function() {
		$('body,html').animate({scrollTop:0},1500);
		}); 
	});


    $(".menu").on("click","a", function (event) {
        event.preventDefault();
        let id  = $(this).attr('href'),
            top = $(id).offset().top;
        $('body,html').animate({scrollTop: top}, 1500);
    });
	
	
	$("#btn1").on({
		mouseenter: function(){
			$("#btn1_hover").show();
			$(this).hide();
		},
		mouseleave: function(){
			$("#btn1_hover").hide();
			$(this).show();
		}
	});

	$("#st1").on({
		mouseenter: function(){
      		document.getElementById("st1").style.color = "red";
		},
		mouseleave: function(){
      		document.getElementById("st1").style.color = "black";
		}
	});
	
	$("#b1-1").on({
		mouseenter: function(){
      		document.getElementById("b1-1").style.opacity = "0.1";
		},
		mouseleave: function(){
      		document.getElementById("b1-1").style.opacity = "0";
		}
	});
	$("#b1-2").on({
		mouseenter: function(){
      		document.getElementById("b1-2").style.opacity = "0.1";
		},
		mouseleave: function(){
      		document.getElementById("b1-2").style.opacity = "0";
		}
	});
	$("#b2-1").on({
		mouseenter: function(){
      		document.getElementById("b2-1").style.opacity = "0.1";
		},
		mouseleave: function(){
      		document.getElementById("b2-1").style.opacity = "0";
		}
	});
	$("#b2-2").on({
		mouseenter: function(){
      		document.getElementById("b2-2").style.opacity = "0.1";
		},
		mouseleave: function(){
      		document.getElementById("b2-2").style.opacity = "0";
		}
	});
	$("#b3-1").on({
		mouseenter: function(){
      		document.getElementById("b3-1").style.opacity = "0.1";
		},
		mouseleave: function(){
      		document.getElementById("b3-1").style.opacity = "0";
		}
	});
	$("#b3-2").on({
		mouseenter: function(){
      		document.getElementById("b3-2").style.opacity = "0.1";
		},
		mouseleave: function(){
      		document.getElementById("b3-2").style.opacity = "0";
		}
	});
	
	$("#btn1_hover").on("click", function (event) {
		let link = document.createElement('a');
		link.setAttribute("href", "pic/logo.png");
		link.setAttribute("download", "logo.png");
		link.click();
		return false;
	});

	let scrolled;
		window.onscroll = function() {
	    	scrolled = window.pageYOffset || document.documentElement.scrollTop;
	    	if(scrolled > 1500){
	    	    $("#m4").css({"filter": "invert(100%)"})
	    	}
	    	if(1450 > scrolled){
	    	    $("#m4").css({"filter": "invert(0%)"})         
	    	}
		}

	$("#slide1").on("click", function (event) {
		event.preventDefault();
		$(".header-content").hide();
		$(".text").hide();
		$("#btn1").hide();
      		$("#iPad").attr("src", "pic/iPad_white.png");
      		document.getElementById("slide1").style.opacity = "1";
      		document.getElementById("slide2").style.opacity = "0.5";
      		document.getElementById("slide3").style.opacity = "0.5";
		$("#store").show();
		$(".text").show();
		$(".header-content").slideToggle();
	});
	$("#slide2").on("click", function (event) {
		event.preventDefault();
		$(".header-content").hide();
		$(".text").hide();
		$("#store").hide();
      		$("#iPad").attr("src", "pic/iPad_white.png");
      		document.getElementById("slide1").style.opacity = "0.5";
      		document.getElementById("slide2").style.opacity = "1";
      		document.getElementById("slide3").style.opacity = "0.5";
		$("#btn1").show();
		$(".text").show();
		$(".header-content").slideToggle();
	});
	$("#slide3").on("click", function (event) {
		event.preventDefault();
		$(".header-content").hide();
		$(".text").hide();
      		$("#iPad").attr("src", "pic/iPad_black.png");
      		document.getElementById("slide1").style.opacity = "0.5";
      		document.getElementById("slide2").style.opacity = "0.5";
      		document.getElementById("slide3").style.opacity = "1";
		$(".text").show();
		$(".header-content").slideToggle();
	});
});