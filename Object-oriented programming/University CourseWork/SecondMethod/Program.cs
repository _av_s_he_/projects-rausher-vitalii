﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Numerics;

namespace Coursework_OOP_IFTUNG
{
    partial class Program
    {
        private const char _tab = '\t';
        private const int 
            _numberOfColumns = 3,
            _nameColumn = 0,
            _parametrOneColumn = 1,
            _parametrTwoColumn = 2,
            _radius = 0,
            _height = 1;

        static void Main(string[] args)
        {
            MyFiles file;
            Figure figure = null;

            string filePath;
            int numberOfRows;

            Console.Write($"Enter opening file name:" + _tab);
            filePath = Console.ReadLine();

            Console.Write($"Enter number of figures in opening file:" + _tab);
            numberOfRows = int.Parse(Console.ReadLine());

            file = new MyFiles(filePath, numberOfRows);
            
            for (int i = 0; i < numberOfRows; i++)
            {
                for (int j = 0; j < _numberOfColumns; j++)
                {
                    Console.Write(file.FileData[i, j] + _tab);
                }
                Console.WriteLine();
            }

            for (int i = 0; i < numberOfRows; i++)
            {
                double[] parametrs;
                switch (file.FileData[i, _nameColumn])
                {
                    case "Sphere":
                        parametrs = GetNumberValue(file.FileData[i, _parametrOneColumn]);
                        figure = new Sphere(parametrs[_radius]);
                        break;

                    case "Cylinder":
                        parametrs = GetNumberValue(file.FileData[i, _parametrOneColumn], file.FileData[i, _parametrTwoColumn]);
                        figure = new Cylinder(parametrs[_radius], parametrs[_height]);
                        Console.WriteLine($"Radius = {parametrs[_radius]} height = {parametrs[_height]}");
                        break;

                    case "Cone":
                        parametrs = GetNumberValue(file.FileData[i, _parametrOneColumn], file.FileData[i, _parametrTwoColumn]);
                        figure = new Cone(parametrs[_radius], parametrs[_height]);
                        break;
                }
                file.WriteFile(i,figure.Answer);
            }
        }
    }
}
