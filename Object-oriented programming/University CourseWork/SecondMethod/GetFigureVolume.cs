﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

namespace Coursework_OOP_IFTUNG
{
    partial class Program
    {
        private const int _parametersCount = 2;
        private const string
            _freeSpace = "",
            _radiusParametr = "radius=",
            _heightParametr = "height=";

        private const char _equalPoint = '=',
            _break = ' ';

        static double[] GetNumberValue(string paramOne, string paramTwo = _freeSpace)
        {
            double[] parametrs = new double[_parametersCount];

            string parametr = paramOne, tmp_str = _freeSpace;

            for (int k = 0; k < _parametersCount; k++)
            {
                for (int i = 0; i < parametr.Length; i++)
                {
                    tmp_str += parametr[i];
                    if (parametr[i] == _equalPoint)
                    {
                        string num = _freeSpace;
                        float x = 0, y = 0, z = 0, ii = 0;
                        for (int j = i + 1; j < parametr.Length; j++)
                        {
                            if (parametr[j] == _break)
                            {
                                switch (ii)
                                {
                                    case 0:
                                        x = float.Parse(num);
                                        num = _freeSpace;
                                        break;
                                    case 1:
                                        y = float.Parse(num);
                                        num = _freeSpace;
                                        break;
                                    case 2:
                                        z = float.Parse(num);
                                        num = _freeSpace;
                                        break;
                                }
                                ii++;
                                continue;
                            }
                            num += parametr[j];
                        }
                        Vector3 vector = new Vector3(x, y, z);
                        if (tmp_str == _radiusParametr)
                        {
                            parametrs[_radius] = vector.Length();
                        }
                        if (tmp_str == _heightParametr)
                        {
                            parametrs[_height] = vector.Length();
                        }
                        break;
                    }
                }
                parametr = paramTwo;
                tmp_str = _freeSpace;
            }
            return parametrs;
        }
    }
}
