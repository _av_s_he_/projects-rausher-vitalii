﻿using System;
using System.IO;
namespace Coursework_OOP_IFTUNG
{
    class MyFiles
    {
        private const int _numberOfColumns = 3;
        private const string _freeSpace = "";
        private const char 
            _checkPoint = ';',
            _newLinePoint = '\n';

        private string 
            _filePath = "File_To_Open.txt",
            _fileAnswersPath = "File_To_Write.txt";
        
        private int _numberOfRows = 0;
        
        public string[,] FileData { get; private set; }
        private string[] EncodedFileData { get; set; }

        public MyFiles(string filePath = "File_To_Open.txt", int numberOfRows = 3)
        {
            this._numberOfRows = numberOfRows;
            this._filePath = filePath;
            OpenFile();
            DecodeFileData();
        }

        public void WriteFile(int condition, string answer)
        {
            string appendText = 
                EncodedFileData[condition] + answer + Environment.NewLine;
            File.AppendAllText(_fileAnswersPath, appendText);
        }

        private void OpenFile()
        {
            string EncodedString = _freeSpace;
            EncodedFileData = new string[_numberOfRows];

            using (StreamReader sr = new StreamReader(_filePath, System.Text.Encoding.Default))
            {
                EncodedString = sr.ReadToEnd();
            }
            PreliminaryDecoding(EncodedString);

        }
        private void PreliminaryDecoding(string EncodedString)
        {
            string asciiString = _freeSpace, decodedText = _freeSpace;
            for (int j = 0, i = 0; i < EncodedString.Length; i++)
            {
                if (Char.IsDigit(EncodedString[i]))
                {
                    asciiString += EncodedString[i];
                }
                else if (asciiString != _freeSpace)
                {
                    if (decodedText.Contains(_newLinePoint) && j < EncodedFileData.Length)
                    {
                        EncodedFileData[j] = decodedText;
                        j++;
                        decodedText = _freeSpace;
                    }
                    decodedText += (char)int.Parse(asciiString);
                    asciiString = _freeSpace;
                }
            }
        }
        private void DecodeFileData()
        {
            this.FileData = new string[_numberOfRows, _numberOfColumns];
            for (int i = 0; i < _numberOfRows; i++)
            {
                string str = _freeSpace;
                for (int jj = 0, j = 0; j < EncodedFileData[i].Length; j++)
                {
                    if (EncodedFileData[i][j] == _checkPoint)
                    {
                        this.FileData[i, jj] = str;
                        str = _freeSpace;
                        jj++;

                        continue;
                    }
                    str += EncodedFileData[i][j];
                }
            }
        }
    }
}
