﻿using System;

namespace Coursework_OOP_IFTUNG
{
    abstract class Figure
    {
        protected double radius;
        public double Volume { get; protected set; }
        public string Answer { get; protected set; }
        protected virtual void GetVolume()
        {
            this.Volume = Math.PI * Math.Pow(radius, 2);
        }
    }
    class Sphere : Figure
    {
        public Sphere(double radius)
        {
            this.radius = radius;
            GetVolume();
            Answer = ($"Sphere volume = {this.Volume}");
        }
        protected override void GetVolume()
        {
            base.GetVolume();
            this.Volume *= (radius * 4) / 3;
        }
    }

    class Cylinder : Figure
    {
        private double _height;
        public Cylinder(double radius, double height)
        {
            this.radius = radius;
            this._height = height;
            GetVolume();
            Answer = ($"Cylinder volume = {this.Volume}");
        }
        protected override void GetVolume()
        {
            base.GetVolume();
            this.Volume *= _height;
        }
    }

    class Cone : Figure
    {
        private double _height;
        public Cone(double radius, double height)
        {
            this.radius = radius;
            this._height = height;
            GetVolume();
            Answer = ($"Cone volume = {this.Volume}");
        }
        protected override void GetVolume()
        {
            base.GetVolume();
            this.Volume *= _height / 3;
        }
    }
}
