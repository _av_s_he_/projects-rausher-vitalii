﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

namespace Coursework_OOP_IFTUNG
{
    partial class Program
    {
        private const int _parametersCount = 2;
        private const string
            _freeSpace = "",
            _radiusParametr = "radius=",
            _heightParametr = "height=";

        private const char _equalPoint = '=';

        static double[] GetNumberValue(string paramOne, string paramTwo = _freeSpace)
        {
            double[] parametrs = new double[_parametersCount];

            string parametr = paramOne, tmp_str = _freeSpace;

            for (int k = 0; k < _parametersCount; k++)
            {
                for (int i = 0; i < parametr.Length; i++)
                {
                    tmp_str += parametr[i];
                    if (parametr[i] == _equalPoint)
                    {
                        string num = _freeSpace;
                        for (int j = i + 1; j < parametr.Length; j++)
                        {
                            num += parametr[j];
                        }
                        if (tmp_str == _radiusParametr)
                        {
                            parametrs[_radius] = double.Parse(num);
                        }
                        if (tmp_str == _heightParametr)
                        {
                            parametrs[_height] = double.Parse(num);
                        }
                        break;
                    }
                }
                parametr = paramTwo;
                tmp_str = _freeSpace;
            }
            return parametrs;
        }
    }
}
