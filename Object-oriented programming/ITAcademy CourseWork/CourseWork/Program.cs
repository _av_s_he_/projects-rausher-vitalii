﻿using System;
using System.Collections.Generic;
using System.Numerics;

namespace ITAcademy_Lab_4
{
    delegate Player SetHero(int weaponOwner, int weaponID);
    partial class Program
    {
       
        static void Main(string[] args)
        {
            bool statusMap = true;
            Random rnd = new Random();
            SetHero setHero = Factory;
            Player player;
            List<Player> enemys = new List<Player>();

            int choose = 0;
            Console.WriteLine($"Choose your favorit character:");
            for (int i = 0; i < 6; i++)
            {
                player = setHero(i, 0);
                Console.WriteLine($"\n[{i}]" + player.SayMyName() + "\n");
                Console.WriteLine($"\t[Health]atSpawn = {player.Health}");
                player.Treatment();
                player.Treatment();
                Console.WriteLine($"\t[Health]:Maximum = {player.Health}\n");
            }
            Console.Write("Choose:\t");
            try
            {
                choose = int.Parse(Console.ReadLine());
                if (choose >= 6)
                {
                    throw new FormatException();
                }
            }
            catch (FormatException)
            {
                Console.WriteLine("Incorrect choose!\nNow you will be a Soldier!\n\nPress any key to Start!");
                choose = 0;
                Console.ReadKey();
            }
            player = setHero(choose, 0);

            GetEnemies(3);

            IPosition positionPlayer = player;

            //Call main game loop
            Map();

            //Main game loop
            void Map()
            {
                if (enemys.Count == 0)
                {
                    GetEnemies(rnd.Next(5));
                }
                int threatmentX = rnd.Next(10);
                int threatmentY = rnd.Next(10);
                while (player.Health > 0)
                {
                    Console.Clear();
                    if (statusMap)
                    {
                        for (int i = 0; i < 10; i++)
                        {
                            for (int ii = 0; ii < 10; ii++)
                            {
                                bool enemyHere = false;

                                foreach (Player enemy in enemys)
                                {
                                    if (i == enemy.position.Y && ii == enemy.position.X)
                                    {
                                        enemyHere = true;
                                    }
                                }
                                if (player.position.Y == i && player.position.X == ii)
                                {
                                    Console.Write($"[X]");
                                }
                                else if (threatmentX == ii && threatmentY == i)
                                {
                                    Console.Write("[T]");
                                }
                                else if (enemyHere)
                                {
                                    Console.Write($"[E]");
                                }
                                else
                                {
                                    Console.Write($"[.]");
                                }
                                CheckForEnemy();
                                if (threatmentX == player.position.X && threatmentY == player.position.Y) 
                                {
                                    player.Treatment();
                                    threatmentX = -1;
                                    threatmentY = -1;
                                }
                            }
                            Console.WriteLine();
                        }
                    }
                    positionPlayer.PlayerMoving(Console.ReadKey(false).Key);
                    PlayerBehindTheWall();
                }
            }

            void Fight(Player enemy)
            {
                bool fighting = true;
                Console.Clear();
                Console.WriteLine("Choose weapon:");
                for (int jj = 0; jj < 3; jj++)
                {
                    player.ChangeWeapon(jj);
                    Console.WriteLine($"\t[{jj}][Weapon]:{player.WeaponName}\t\t[damage]:{player.Damage}");
                }
                Console.Write("Choose:\t");
                try
                {
                    choose = int.Parse(Console.ReadLine());
                    if (choose >= 3)
                    {
                        throw new FormatException();
                    }
                }
                catch (FormatException)
                {
                    Console.WriteLine("Incorrect choose!\nFight with Hands!\nNice choise i guess =)\n\nPress any key to Fight!");
                    choose = 0;
                    Console.ReadKey();

                }
                player.ChangeWeapon(choose);

                //fighting loop
                while (fighting)
                {
                    Console.Clear();

                    if (enemy.Health <= 0)
                    {
                        enemys.Remove(enemy);

                        Map();
                    }

                    Console.WriteLine($"{player.SayMyName()} [HP:{player.Health}] with {player.WeaponName}: [{player.WeaponAmmo}ammos for {player.Damage} damage]");
                    Console.WriteLine($"\n\t\t\tvs\n");
                    Console.WriteLine($"{enemy.SayMyName()} [HP:{enemy.Health}] with {enemy.WeaponName}: [{enemy.WeaponAmmo}ammos for {enemy.Damage} damage]");

                    Console.WriteLine($"\n[Spase] to Shoot the enemy\n" +
                        $"[Enter] to try to dodge from enemy\n" +
                        $"[Backspace] to reload the weapon");
                    switch (Console.ReadKey().Key)
                    {
                        case ConsoleKey.Spacebar:
                            Console.WriteLine($"\n--------------------------[Enemy]----------------------------------");
                            Console.WriteLine(enemy.ShootedBy(player, false));
                            Console.WriteLine($"\n--------------------------[Player]---------------------------------");
                            Console.WriteLine($"After the shootout, you have left: [{player.WeaponAmmo}] ammo");
                            Console.WriteLine($"\n--------------------------[Player]---------------------------------");
                            Console.WriteLine(player.ShootedBy(enemy, false));
                            Console.WriteLine($"\n--------------------------[Enemy]----------------------------------");
                            Console.WriteLine($"After the shootout, enemy have left: [{enemy.WeaponAmmo}] ammo");
                            break;
                        case ConsoleKey.Enter:
                            Console.WriteLine($"\n--------------------------[Player]---------------------------------");
                            Console.WriteLine(player.ShootedBy(enemy, true));
                            Console.WriteLine($"\n--------------------------[Enemy]----------------------------------");
                            Console.WriteLine($"After the shootout, enemy have left: [{enemy.WeaponAmmo}] ammo");
                            break;
                        case ConsoleKey.Backspace:
                            Console.WriteLine($"\n--------------------------[Player]---------------------------------");
                            Console.WriteLine(player.ShootedBy(enemy, true));
                            Console.WriteLine($"\n--------------------------[Enemy]----------------------------------");
                            Console.WriteLine($"After the shootout, enemy have left: [{enemy.WeaponAmmo}] ammo");
                            player.Reload();
                            Console.WriteLine($"\n--------------------------[Player]---------------------------------");
                            Console.WriteLine($"After reload magazine: [{player.WeaponAmmo}] ammo");
                            break;
                        default:
                            Console.WriteLine($"\n--------------------------[Player]---------------------------------");
                            Console.WriteLine(player.ShootedBy(enemy, false));
                            Console.WriteLine($"\n--------------------------[Enemy]----------------------------------");
                            Console.WriteLine($"After the shootout, enemy have left: [{enemy.WeaponAmmo}] ammo");
                            break;
                    }
                    if (enemy.WeaponAmmo <= 0)
                    {
                        Console.WriteLine($"\n--------------------------[Enemy]----------------------------------");
                        enemy.Reload();
                        Console.WriteLine($"After reload magazine: [{enemy.WeaponAmmo}] ammo");
                    }
                    //close game if you lose
                    if (player.Health <= 0)
                    {
                        fighting = false;
                        Console.WriteLine("You be killed by enemy!");
                        System.Environment.Exit(0);
                    }
                    Console.WriteLine($"\n\nPress any key to continue!");
                    Console.ReadKey();
                  }
                //return to map after win
                Map();
            }
            //add to enemy list new Players
            void GetEnemies(int count)
            {
                for (int i = 0; i < count; i++)
                {
                    enemys.Add(setHero(rnd.Next(5), rnd.Next(3)));
                    enemys[i].position = new Vector2(rnd.Next(0, 10), rnd.Next(0, 10));
                }
            }
            void CheckForEnemy()
            {
                foreach (Player enemy in enemys)
                {
                    if (player.position == enemy.position)
                    {
                        Fight(enemy);
                    }
                }
            }
            //create a wall aboute map
            void PlayerBehindTheWall()
            {
                if (player.position.X < 0)
                {
                    player.position = new Vector2(0, player.position.Y);
                }
                if (player.position.Y < 0)
                {
                    player.position = new Vector2(player.position.X, 0);
                }
                if (player.position.X > 9)
                {
                    player.position = new Vector2(9, player.position.Y);
                }
                if (player.position.Y > 9)
                {
                    player.position = new Vector2(player.position.X, 9);
                }
            }
        }
    }
}
