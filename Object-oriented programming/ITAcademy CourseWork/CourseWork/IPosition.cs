﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

namespace ITAcademy_Lab_4
{
    interface IPosition
    {
        Vector2 position { get; set; }
        /// <summary>
        /// Move character on map
        /// </summary>
        /// <param name="key">Pressed key</param>
        void PlayerMoving(ConsoleKey key)
        {
            switch (key)
            {
                case ConsoleKey.DownArrow:
                    position = new Vector2(position.X, position.Y + 1);
                    break;
                case ConsoleKey.UpArrow:
                    position = new Vector2(position.X, position.Y - 1);
                    break;
                case ConsoleKey.LeftArrow:
                    position = new Vector2(position.X - 1, position.Y);
                    break;
                case ConsoleKey.RightArrow:
                    position = new Vector2(position.X + 1, position.Y);
                    break;
            }
        }
    }
}
