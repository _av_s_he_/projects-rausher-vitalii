﻿using System;
using System.IO;

namespace ITAcademy_Lab_4
{
    public class Weapons : IShoot
    {
        private const string _filePath = "ClassWeapons.ra";
        private const int _numberOfRows = 8;
        private const int _numberOfColumns = 4;
        private const string _freeSpace = "";
        private const char _checkPoint = ';';
        private const char _newLinePoint = '\n';


        private const int
            _attackerWaponNameColumn = 0,
            _attackerWeaponAmmoColumn = 1,
            _attackerWeaponShootColumn = 2,
            _attackerSpecialWeapon = 2,
            _attackerDamageColumn = 3;

        private string[,] FileData;
        private string[] EncodedFileData { get; set; }
        private int WeaponMaxAmmo { get; set; }
        private int WeaponID { get; set; }
        private int WeaponOwner { get; set; }
        private int Damage { get; set; }
        int IShoot.Damage { get { return Damage; } }
        public string WeaponName { get; set; }
        public int WeaponAmmo { get; set; }

        /// <summary>
        /// Weapon constructor
        /// </summary>
        /// <param name="weaponOwner">OwnerID of weapon</param>
        /// <param name="weaponId">Id of weapon (1-3)</param>
        public Weapons(int weaponOwner, int weaponId)
        {
            OpenFile();
            DecodeFileData();

            this.WeaponOwner = weaponOwner;
            ChangeWeapon(weaponId);
        }
        /// <summary>
        /// Minus weapon ammo
        /// </summary>
        void IShoot.Shoot()
        {
            WeaponAmmo -= int.Parse(this.FileData[WeaponID, _attackerWeaponShootColumn]);
        }
        /// <summary>
        /// Weapon ammo reload
        /// </summary>
        void IShoot.Reload()
        {
            WeaponAmmo = WeaponMaxAmmo;
        }
        /// <summary>
        /// Changing weapon id in hand of player
        /// </summary>
        /// <param name="weaponID">id of weapon to change</param>
        public void ChangeWeapon(int weaponID)
        {
            this.WeaponID = weaponID;

            if (this.WeaponID == _attackerSpecialWeapon)
            {
                this.WeaponID += this.WeaponOwner;
            }

            WeaponName = this.FileData[this.WeaponID, _attackerWaponNameColumn];
            WeaponAmmo = int.Parse(this.FileData[this.WeaponID, _attackerWeaponAmmoColumn]);
            Damage = int.Parse(this.FileData[this.WeaponID, _attackerDamageColumn]);
            WeaponMaxAmmo = WeaponAmmo;
        }
        /// <summary>
        /// Open and preliminary decoding file into file text
        /// </summary>
        private void OpenFile()
        {
            string EncodedString = _freeSpace;
            EncodedFileData = new string[_numberOfRows];

            using (StreamReader sr = new StreamReader(_filePath, System.Text.Encoding.Default))
            {
                EncodedString = sr.ReadToEnd();
            }
            PreliminaryDecoding(EncodedString);
            
        }
        /// <summary>
        /// Preliminary decoding file into file text
        /// </summary>
        /// <param name="EncodedString">string from file to encode</param>
        private void PreliminaryDecoding(string EncodedString)
        {
            string asciiString = _freeSpace, decodedText = _freeSpace;
            for (int j = 0, i = 0; i < EncodedString.Length; i++)
            {
                if (Char.IsDigit(EncodedString[i]))
                {
                    asciiString += EncodedString[i];
                }
                else if (asciiString != _freeSpace)
                {
                    if (decodedText.Contains(_newLinePoint))
                    {
                        EncodedFileData[j] = decodedText;
                        j++;
                        decodedText = _freeSpace;
                    }
                    decodedText += (char)int.Parse(asciiString);
                    asciiString = _freeSpace;
                }
            }
        }
        /// <summary>
        /// Get array from decoded file
        /// </summary>
        private void DecodeFileData()
        {
            this.FileData = new string[_numberOfRows,_numberOfColumns];
            for (int i = 0; i < _numberOfRows; i++)
            {
                string str = _freeSpace;
                for (int jj = 0, j = 0; j < EncodedFileData[i].Length; j++)
                {
                    if (EncodedFileData[i][j] == _checkPoint)
                    {
                        this.FileData[i, jj] = str;
                        str = _freeSpace;
                        jj++;

                        continue;
                    }
                    str += EncodedFileData[i][j];
                }
            }
        }
    }
}
