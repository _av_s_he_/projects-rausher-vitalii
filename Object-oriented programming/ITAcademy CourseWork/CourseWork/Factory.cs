﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ITAcademy_Lab_4
{
    partial class Program
    {
        /// <summary>
        /// Set constructor for GameObject
        /// </summary>
        /// <param name="i">choose a character</param>
        /// <param name="j">choose a weapon</param>
        /// <return>object GameObject</returns>
        static private Player Factory(int i, int j)
        {
            switch (i)
            {
                case 0:
                    return new Soldier(j);
                case 1:
                    return new FireMan(j);
                case 2:
                    return new Bomber(j);
                case 3:
                    return new Engineer(j);
                case 4:
                    return new Sniper(j);
                case 5:
                    return new Medic(j);
                default:
                    return null;
            }
        }
    }
}
