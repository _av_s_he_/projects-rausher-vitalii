﻿using ITAcademy_Lab_4;
using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

namespace ITAcademy_Lab_4
{
    abstract class Player : IShoot, IGameObjects, IPosition
    {
        protected Weapons WeaponInHand;
        protected int MaxHealth { get; set; }
        public int Health { get; protected set; }
        public int Damage => ((IShoot)WeaponInHand).Damage;

        public string WeaponName => ((IShoot)WeaponInHand).WeaponName;

        public int WeaponAmmo => ((IShoot)WeaponInHand).WeaponAmmo;

        public Vector2 position { get; set; }

        public abstract string SayMyName();

        public void Shoot()
        {
            ((IShoot)WeaponInHand)?.Shoot();
        }
        public void Reload()
        {
            ((IShoot)WeaponInHand)?.Reload();
        }
        public string ShootedBy(Player shooter, bool dodge)
        {
            if (shooter.WeaponAmmo > 0)
            {
                shooter.Shoot();
                const int _percentNumber = 10;
                int _unluckShootPercent = 3;
                if (dodge) { _unluckShootPercent *= 2; }
                Random rd = new Random();
                if (rd.Next(0, _percentNumber) > _unluckShootPercent)
                {
                    this.Health -= shooter.Damage;
                    return $"{SayMyName()} have been shooted by {shooter.SayMyName()} from [{shooter.WeaponName}] for {shooter.Damage}hp\n{SayMyName()} health = {Health}";
                }
                return $"{SayMyName()} are lucky, the bullet flew past him";
            }
            return "Must to reload!";
        }
        public void ChangeWeapon(int newWeaponID)
        {
            WeaponInHand.ChangeWeapon(newWeaponID);
        }
        public abstract void Treatment();

    }
}