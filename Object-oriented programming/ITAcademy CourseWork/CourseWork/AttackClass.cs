﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ITAcademy_Lab_4
{
    abstract class AttackClass : Player
    {
        public override void Treatment()
        {
            if (MaxHealth - Health >= 85)
            {
                this.Health += 85;
            } else if (Health < MaxHealth)
            {
                this.Health = MaxHealth;
            }
        }
        public override string SayMyName()
        {
            return "[Attacker]:";
        }
    }
    class Soldier : AttackClass
    {
        private const int _soldierID = 0;
        public Soldier(int weaponID = 0)
        {
            this.Health = 100;
            MaxHealth = 185;
            this.WeaponInHand = new Weapons(_soldierID, weaponID);
        }
        public override string SayMyName()
        {
            return (base.SayMyName() +  "Soldier");
        }

    }
    class FireMan : AttackClass
    {

        private const int _fireManID = 1;
        public FireMan(int weaponID = 0)
        {
            this.Health = 175;
            MaxHealth = 260;
            this.WeaponInHand = new Weapons(_fireManID,weaponID);
        }
        public override string SayMyName()
        {
            return (base.SayMyName() + "Arsonist");
        }

    }
}
