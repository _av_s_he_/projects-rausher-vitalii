﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ITAcademy_Lab_4
{
    abstract class SupportClass : Player
    {
        public override void Treatment()
        {
            if (MaxHealth - Health >= 75)
            {
                this.Health += 75;
            }
            else if (Health < MaxHealth)
            {
                this.Health = MaxHealth;
            }
        }
        public override string SayMyName()
        {
            return "[Supporter]:";
        }
    }
    class Sniper : SupportClass
    {
        
        private const int _sniperID = 4;
        public Sniper(int weaponID = 0)
        {
            this.Health = 110;
            MaxHealth = 185;
            this.WeaponInHand = new Weapons(_sniperID,weaponID);
        }
        public override string SayMyName()
        {
            return (base.SayMyName() + "Sniper");
        }

    }
    class Medic : SupportClass
    {
        private const int _medicID = 5;
        public Medic(int weaponID = 0)
        {
            this.Health = 150;
            MaxHealth = 225;
            this.WeaponInHand = new Weapons(_medicID,weaponID);
        }
        public override string SayMyName()
        {
            return (base.SayMyName() + "Medic");
        }
    }
}
