﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ITAcademy_Lab_4
{
    abstract class DefenseClass : Player
    {
        public override void Treatment()
        {
            if (MaxHealth - Health >= 60)
            {
                this.Health += 60;
            }
            else if (Health < MaxHealth)
            {
                this.Health = MaxHealth;
            }
        }
        public override string SayMyName()
        {
            return "[Defenser]:";
        }
    }

    class Bomber : DefenseClass
    {
        private const int _bomberID = 2;
        public Bomber(int weaponID = 0)
        {
            this.Health = 175;
            MaxHealth = 235;
            this.WeaponInHand = new Weapons(_bomberID,weaponID);
        }
        public override string SayMyName()
        {
            return (base.SayMyName() + "Bomber");
        }

    }
    class Engineer : DefenseClass
    {
        private const int _engineerID = 3;
        public Engineer(int weaponID = 0)
        {
            this.Health = 125;
            MaxHealth = 185;
            this.WeaponInHand = new Weapons(_engineerID,weaponID);
        }
        public override string SayMyName()
        {
            return (base.SayMyName() +  "Engineer");
        }
    }
}
