﻿namespace ITAcademy_Lab_4
{
    interface IGameObjects
    {
        int Damage { get; }
        int Health { get; }
        /// <summary>
        /// Change weapon in hand
        /// </summary>
        /// <param name="newWeaponID">new weapon id in hand</param>
        void ChangeWeapon(int newWeaponID);
        /// <summary>
        /// Reload weapon
        /// </summary>
        void Reload();
        /// <summary>
        /// Name of player character
        /// </summary>
        /// <returns>string contains selected character name</returns>
        string SayMyName();
        /// <summary>
        /// Decreasing weapon ammo count
        /// </summary>
        void Shoot();
        /// <summary>
        /// Call Shoot() function and decreasing hp in object
        /// </summary>
        /// <param name="gm">GameObject of shooter</param>
        /// <param name="dodge">Вoubles the probability of parsing if true</param>
        /// <returns>string with details</returns>
        string ShootedBy(Player gm, bool dodge);
        /// <summary>
        /// Heal character
        /// </summary>
        void Treatment();
    }
}