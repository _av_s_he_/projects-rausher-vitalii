﻿using System;

namespace ITAcademy_Lab_4
{
    interface IShoot
    {
        string WeaponName { get; }
        int WeaponAmmo { get; }
        int Damage { get; }
        void Shoot();
        void Reload();
    }
}
